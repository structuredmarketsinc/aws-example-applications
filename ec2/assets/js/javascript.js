jQuery(document).ready(function($){  
    
    //If rgion select box value changed
    if( $("#frm_ec2 #region").length ) {
        $(document).on("change", '#frm_ec2 #region', function(e) { 
            //Get AMIs
            ec2_get_amis_html($(this));
            ec2_get_ssh_html($(this));
        });
    }
    
    //If AMI select box value changed
    if( $("#frm_ec2 #ami").length ) {
        $(document).on("change", '#frm_ec2 #ami', function(e) { 
            //Get Instance Types
            ec2_get_instance_types_html($(this));
        });
    }
    
    //If SSH key select box value changed
    if( $("#frm_ec2 #ssh").length ) {
        $(document).on("change", '#frm_ec2 #ssh', function(e) {             
            if( $(this).val() == "ec2_create_new_ssh" ) {
                $("#ssh_new").show();
            }else{
                $("#ssh_new").val("");
                $("#ssh_new").hide();
            }
        });
    }
});

/*
 * Show SSH key select box
 */
function ec2_get_ssh_html( obj ) {
    
    var $ = jQuery;
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: ec2_data.ajaxurl,
        data: {
                action: 'ec2_get_ssh_html',
                region: $("#frm_ec2 #region").val()
        },
        success: function(response) {
            if(response.error) {
                alert(response.message);
            }else{
                $("#ssh").html(response.html);
            }
        },
        error: function (xhr, status, error) {
            alert("There is some error to connect with server. Please try again.");
        }
    }).always(function(){
               
    });
}

/*
 * Show Instance Types select box
 */
function ec2_get_instance_types_html( obj ) {
    
    var $ = jQuery;
    show_ajax_loader(obj);
    $(".show_instance_type").hide();
    $(".showif_instance_type").hide();
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: ec2_data.ajaxurl,
        data: {
                action: 'ec2_get_instance_types_html',
                region: $("#frm_ec2 #region").val()                       
        },
        success: function(response) {
            if(response.error) {
                alert(response.message);
            }else{
                $(".show_instance_type select").html(response.html);
                $(".show_instance_type").show();
                $(".showif_instance_type").show();
            }
        },
        error: function (xhr, status, error) {
            alert("There is some error to connect with server. Please try again.");
        }
    }).always(function(){
        hide_ajax_loader(obj);                
    });
}

/*
 * Show AMIs select box
 */
function ec2_get_amis_html( obj ) {
    
    var $ = jQuery;
    show_ajax_loader(obj);
    $(".show_amis").hide();
    $(".show_instance_type").hide();
    $(".showif_instance_type").hide();
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: ec2_data.ajaxurl,
        data: {
                action: 'ec2_get_amis_html',
                region: $("#frm_ec2 #region").val()                     
        },
        success: function(response) {
            if(response.error) {
                alert(response.message);
            }else{
                $(".show_amis select").html(response.html);
                $(".show_amis").show();
            }
        },
        error: function (xhr, status, error) {
            alert("There is some error to connect with server. Please try again.");
        }
    }).always(function(){
        hide_ajax_loader(obj);                
    });
}

//Show Ajax loader
function show_ajax_loader( obj ) {
    
    if(!obj.next('.ec2_ajax_loader').length) {
        obj.after('<div class="ec2_ajax_loader"></div>');
        obj.next('.ec2_ajax_loader').show();
    }
}

//Remove Ajax loader
function hide_ajax_loader( obj ) {
    
    if(obj.next('.ec2_ajax_loader').length) {
        obj.next('.ec2_ajax_loader').remove();
    }
}

//Delete multiple/all objects on listing page
function ec2_delete_all_selected(obj) {
    var $ = jQuery;
    var page = $(obj).attr("data-page");
    if(confirm('Are you sure you want to delete all selected items?')) {
        var ids = "";
        $(".ec2_select_item").each(function(){
            if($(this).is(":checked")){
                if(ids=="") {
                    ids = $(this).val();
                }else{
                    ids = ids+","+$(this).val();
                }
            }
        });
        if(ids=="") {
            alert("You must select some items to delete.");
        }else{
            window.location.href="admin.php?page="+page+"&action=del&ids="+ids;
        }
    }
    return false;
}

//Select all objects on listing page
function ec2_select_all(obj) {
    var $ = jQuery;
    if($(obj).is(":checked")) {
        $(".ec2_select_item").prop("checked",true);
    }else{
        $(".ec2_select_item").prop("checked",false);
    }
}