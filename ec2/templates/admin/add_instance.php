<?php if ( $message!="" ) { echo $message; }?>
<div class="wrap">
<h2><?php echo __( 'Add Instance', 'ec2' );?></h2>
<table class="wp-list-table widefat fixed" cellspacing="0">
	<thead>
        <tr>
            <th scope="col" class="manage-column" style=""><?php echo __( 'Instance Detail', 'ec2' );?></th>
        </tr>
	</thead>
	<tbody id="the-list">
        <tr>
            <td>
            	<form method="post" name="frm_ec2" id="frm_ec2" class="frm_ec2" action="?page=ec2_instances&action=add" enctype="multipart/form-data">
                <table width="100%">
                    <tr>
                    	<td width="180"><?php echo __( 'Region', 'ec2' );?></td>
                        <td>
                            <select name="region" id="region">
                                <option value=""><?php echo __( 'Select Region', 'ec2' );?></option>
                                <?php
                                if(is_array($regions)) {
                                    foreach( $regions as $region ) {
                                        echo '<option value="'.$region['RegionName'].'">'.$region['RegionName'].'</option>';                                        
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <tr class="hidden show_amis">
                    	<td><?php echo __( 'Amazon Machine Image (AMI)', 'ec2' );?></td>
                        <td>
                            <select name="ami" id="ami">
                                <option value=""><?php echo __( 'Select AMI', 'ec2' );?></option>                                
                            </select>
                        </td>
                    </tr>
                    <tr class="hidden show_instance_type">
                    	<td><?php echo __( 'Instance Type', 'ec2' );?></td>
                        <td>
                            <select name="instance_type" id="instance_type">
                                <option value=""><?php echo __( 'Select Instance Type', 'ec2' );?></option>                                
                            </select>
                            <p><?php echo __( 'Except Free Tire Instance Types; for all other types, please make sure that your account is eligible for that instance type.', 'ec2' );?></p>
                        </td>
                    </tr>
                    <tr class="hidden showif_instance_type">
                    	<td><?php echo __( 'Size (in GB)', 'ec2' );?></td>
                        <td>
                            <input type="number" name="size" id="size" value="" min="1" />
                        </td>
                    </tr>
                    <tr class="hidden showif_instance_type">
                    	<td><?php echo __( 'SSH Key Name', 'ec2' );?></td>
                        <td>
                            <select name="ssh" id="ssh">
                                <option value=""><?php echo __( 'Select Key', 'als' );?></option>
                            </select>
                            <input type="text" class="hidden" name="ssh_new" id="ssh_new" value="" placeholder="<?php echo __( 'Put key name here', 'als' );?>" />                            
                        </td>
                    </tr>
                    <tr class="hidden showif_instance_type">
                        <td></td>
                        <td><input type="submit" name="btnsave" id="btnsave" value="<?php echo __( 'Add Instance', 'ec2' );?>" class="button button-primary">
                        </td>
                    </tr>
                </table>
                </form>
            </td>
        </tr>
     </tbody>
</table>
</div>