<?php if ( $message!="" ) { echo $message; }?>
<div class="wrap">
<h2><?php echo __( 'Settings', 'ec2' );?></h2>
<table class="wp-list-table widefat fixed" cellspacing="0">
	<thead>
        <tr>
            <th scope="col" class="manage-column" style=""><?php echo __( 'Settings', 'ec2' );?></th>
        </tr>
	</thead>
	<tbody id="the-list">
        <tr>
            <td>
            	<form method="post" name="frm_ec2" id="frm_ec2" class="frm_ec2" action="?page=ec2_settings" enctype="multipart/form-data">
                <table width="100%">
                    <tr>
                    	<td width="180"><?php echo __( 'AWS Access Key Id', 'ec2' );?></td>
                        <td>
                            <input type="text" name="aws_access_key_id" id="aws_access_key_id" value="<?php echo $aws_access_key_id;?>" />
                        </td>
                    </tr>
                    <tr>
                    	<td><?php echo __( 'AWS Secret Key', 'ec2' );?></td>
                        <td>
                            <input type="text" name="aws_secret_key" id="aws_secret_key" value="<?php echo $aws_secret_key;?>" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td></td>
                        <td><input type="submit" name="btnsave" id="btnsave" value="<?php echo __( 'Update', 'ec2' );?>" class="button button-primary">
                        </td>
                    </tr>
                </table>
                </form>
            </td>
        </tr>
     </tbody>
</table>
</div>