<?php if ( $message!="" ) { echo $message; }?>
<style>
    .ec2_view_icon{margin-left:10px;width: 32px;height: 16px;font-size: 0px;background:url("<?php echo EC2_PLUGIN_URL;?>assets/images/view.png") no-repeat;display: inline-block;}
    .ec2_edit_icon{margin-left:10px;width: 24px;height: 24px;font-size: 0px;background:url("<?php echo EC2_PLUGIN_URL;?>assets/images/edit.png") no-repeat;display: inline-block;}
    .ec2_del_icon{margin-left:10px;width: 24px;height: 24px;font-size: 0px;background:url("<?php echo EC2_PLUGIN_URL;?>assets/images/del.png") no-repeat;display: inline-block;}
    .ec2_pdf_icon{margin-left:10px;width: 24px;height: 24px;font-size: 0px;background:url("<?php echo EC2_PLUGIN_URL;?>assets/images/pdf.png") no-repeat;display: inline-block;}
</style>
<div class="wrap">
    <br>
        <h1><?php echo __( 'Instances', 'ec2' );?> <a href="?page=ec2_instances&action=add" class="page-title-action">Add New</a></h1>
    <br>
    <div id="FieldsTableContainer" style="width: 100%;"></div>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('#FieldsTableContainer').jtable({
                title: 'Manage All Instances',
                paging: true,
                pageSize: 20,
                sorting: true,
                defaultSorting: 'ID ASC',
                actions: {
                    listAction: ajaxurl+'?action=ec2_list_instances'
                },
                fields: {
                        Action: {
                            title: 'Actions',
                            width: '20px',
                            sorting: false
                        },
                        ID: {
                            title: 'ID',
                            key: true,
                            create: false,
                            edit: false,
                            list: true,
                            width: '40px'
                        },
                        State: {
                            title: 'Current State',
                            width: '15%'
                        },
                        Region: {
                            title: 'Region',
                            width: '20%'
                        },
                        Size: {
                            title: 'Size',
                            width: '15%'
                        },
                        SSH: {
                            title: 'SSH Key',
                            width: '20%'
                        },
                        InstanceType: {
                            title: 'Instance Type',
                            width: '20%'
                        }
                }
            });
            //Load person list from server
            jQuery('#FieldsTableContainer').jtable('load');
            window.history.pushState("", "", '?page=ec2_instances');
        });
    </script>            
</div>