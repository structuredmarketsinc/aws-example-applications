<?php
/*
Plugin Name: Amazon EC2 API
Description: EC2 API
Version: 1.0.0
Author: Muhammad Atiq
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
    echo "Hi there!  I'm just a plugin, not much I can do when called directly.";
    exit;
}

define( 'EC2_PLUGIN_NAME', 'EC2' );
define( 'EC2_PLUGIN_PATH', plugin_dir_path(__FILE__) );
define( 'EC2_PLUGIN_URL', plugin_dir_url(__FILE__) );
define( 'EC2_SITE_BASE_URL',  rtrim(get_bloginfo('url'),"/")."/");
define( 'EC2_LANG_DIR', dirname( plugin_basename(__FILE__) ).'/language/' );

require_once EC2_PLUGIN_PATH.'includes/main.php';

register_activation_hook( __FILE__, array( 'EC2', 'ec2_install' ) );
register_deactivation_hook( __FILE__, array( 'EC2', 'ec2_uninstall' ) );