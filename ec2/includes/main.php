<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Plugin main class that will control the whole skeleton and common functions
 *
 * PHP version 5
 *
 * @category   Main
 * @package    EC2 API
 * @author     Muhammad Atiq
 * @version    1.0.0
 * @since      File available since Release 1.0.0
*/

use Aws\Ec2\Ec2Client;

class EC2
{
     
    //Plugin starting point. Will call appropriate actions
    public function __construct() {

        add_action( 'plugins_loaded', array( $this, 'ec2_init' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'ec2_enqueue_scripts' ), 10 );
        add_action( 'admin_enqueue_scripts', array( $this, 'ec2_enqueue_scripts' ), 10 );
    }

    //Plugin initialization
    public function ec2_init() {

        do_action('ec2_before_init');
        
        global $ec2_options,$ec2_lang;
        
        $ec2_options = get_option( 'ec2_settings' );
        load_plugin_textdomain( 'ec2', FALSE, EC2_LANG_DIR );        
        
        if(is_admin()){            
            require_once EC2_PLUGIN_PATH.'includes/admin.php';            
        }

        do_action('ec2_after_init');
    }
    
    //Function will add CSS and JS files
    public function ec2_enqueue_scripts() {
        
        do_action('ec2_before_enqueue_scripts');
        
        wp_enqueue_script( 'jquery' );
        wp_register_script( 'ec2_jtable_js', EC2_PLUGIN_URL. "assets/js/jtable/jquery.jtable.js", array( 'jquery-ui-widget', 'jquery-ui-mouse', 'jquery-ui-position', 'jquery-ui-accordion', 'jquery-ui-autocomplete', 'jquery-ui-button', 'jquery-ui-datepicker', 'jquery-ui-dialog', 'jquery-ui-draggable', 'jquery-ui-droppable', 'jquery-effects-blind', 'jquery-effects-bounce', 'jquery-effects-clip', 'jquery-effects-drop', 'jquery-effects-explode', 'jquery-effects-fade', 'jquery-effects-fold', 'jquery-effects-highlight', 'jquery-effects-pulsate', 'jquery-effects-scale', 'jquery-effects-shake', 'jquery-effects-slide', 'jquery-effects-transfer', 'jquery-ui-menu', 'jquery-ui-progressbar', 'jquery-ui-resizable', 'jquery-ui-selectable', 'jquery-ui-slider', 'jquery-ui-sortable', 'jquery-ui-spinner', 'jquery-ui-tabs', 'jquery-ui-tooltip' ), time() );
        wp_register_style( 'ec2_jtable_css', EC2_PLUGIN_URL. "assets/js/jtable/themes/lightcolor/gray/jtable.css" );        
        wp_enqueue_script( 'ec2_js', EC2_PLUGIN_URL.'assets/js/javascript.js', array( 'jquery' ), time() );
        wp_enqueue_style( 'ec2_css', EC2_PLUGIN_URL.'assets/css/style.css', array(), time() );
        
        wp_localize_script( 'ec2_js', 'ec2_data', array(
                            'ajaxurl' => admin_url( 'admin-ajax.php' )
                            ));
        
        do_action('ec2_after_enqueue_scripts');
    }
    
    /*
     * Function to create an instance
     * 
     * @param $region string AWS region e.g. us-west-1
     * @param $key_name string public key name that should be linked e.g. TestKey
     * @param $instance_type string instance type e.g. t2.micro, m1.micro etc instance type should be from the region specified
     * @param $disk_size interger disk size in GBs. e.g. 80
     * @param $image_id string AMI image ID e.g. ami-0e01ce4ee18447327 AMI should belongs to the region specified
     * @param $device_name string Device name, Device name should be as provided in the image AMI
     * 
     * @return False|$instance AWS instance object
     */
    public function ec2_create_instance( $region="", $key_name = "", $instance_type="t2.micro", $disk_size=80, $image_id="", $device_name="/dev/xvda", $create_key = true ) {
        
        if( empty($region) || empty($disk_size) || empty($key_name) ) {
            return false;
        }
        
        //Get EC2 client
        $ec2Client = $this->ec2_get_client( $region );
        if( !$ec2Client ) {
            return false;
        }
        
        $instance = array();
        
        $params['ImageId']              =   $image_id;
        $params['MinCount']             =   1;
        $params['MaxCount']             =   1;
        $params['InstanceType']         =   $instance_type;
        $params['Monitoring']           =   array( 'Enabled'=>true );
        $params['BlockDeviceMappings']  =   array(
                                                array(
                                                    'DeviceName' => $device_name,
                                                    'Ebs' => array(
                                                        'DeleteOnTermination' => true,
                                                        'Encrypted' => false,
                                                        'VolumeSize' => $disk_size,
                                                        'VolumeType' => 'standard',
                                                    )
                                                )
                                            );
        
        
        if( !empty($key_name) ) {
            //Santize the key name to create a file name
            $key_name = sanitize_title($key_name);
            $private_key_path    = $this->ec2_get_private_key( $key_name, $region );
            $params['KeyName']   =   $key_name;
        }
        
        //Create Instance
        $result = $ec2Client->runInstances($params);
        $statusCode = $result['@metadata']['statusCode'];
        if( $statusCode == 200 && is_array($result['Instances']) ) {
            $instance            = $result['Instances'][0];
            if( $create_key ) {
                $instance['KeyPath'] = $private_key_path;
            }
        }
        
        return $instance;
    }
    
    /*
     * Function to get all Images
     * 
     * @param $region string AWS region
     * @param $limit integer number of AMI to get
     * 
     * @return $images all AMIs objects
     */
    public function ec2_get_available_amis( $region = "", $limit = 50 ) {
        
        if( empty($region) ) {
            return false;
        }
        
        //Check if saved in cache
        $amis = get_transient( 'ec2_amis_for_'.$region );
        if( $amis ) {
            return $amis;
        }
        
        //Set initial value
        $images  =array();
        //Get EC2 client
        $ec2Client = $this->ec2_get_client( $region );
        
        if( $ec2Client ) {
            $result = $ec2Client->describeImages([
                        'Owners' => array( 'amazon' ),
                        'Filters' => [
                                    [
                                        'Name' => 'state',
                                        'Values' => ['available'],
                                    ],
                                    [
                                        'Name' => 'image-type',
                                        'Values' => ['machine'],
                                    ],
                                    [
                                        'Name' => 'architecture',
                                        'Values' => ['x86_64'],
                                    ],
                                ]
                    ]);
            $statusCode = $result['@metadata']['statusCode'];
            if( $statusCode == 200 && is_array($result['Images']) ) {
                foreach( $result['Images'] as $image ) {
                    if( isset($image['Description']) ) {
                        $images[] = $image;
                        if(sizeof($images) >= $limit ) {
                            break;
                        }
                    }
                }
                set_transient( 'ec2_amis_for_'.$region, $images, 86400 );
            }
        }
        
        return $images;
    }
    
    /*
     * Function to get all instance types
     * 
     * @param $region string AWS region
     * 
     * @return $types array of all instance types
     */
    public function ec2_get_instance_types( $region = "" ) {
        
        //Set initial value
        $types  =array();
        //Get EC2 client
        $ec2Client = $this->ec2_get_client( $region );
        
        if( $ec2Client ) {
            $result = $ec2Client->describeInstanceTypes([
                'Filters' => [
                                    [
                                        'Name' => 'free-tier-eligible',
                                        'Values' => ['true'],
                                    ]
                                ]
            ]);
            
            $statusCode = $result['@metadata']['statusCode'];
            //If API request successful
            if( $statusCode == 200 ) {
                $free_tire_types = $result['InstanceTypes']; 
                $types['free_tire'] = $free_tire_types;
            }
            
            $result = $ec2Client->describeInstanceTypes([
                'Filters' => [
                                    [
                                        'Name' => 'free-tier-eligible',
                                        'Values' => ['false'],
                                    ]
                                ]
            ]);
            
            $statusCode = $result['@metadata']['statusCode'];
            //If API request successful
            if( $statusCode == 200 ) {
                $free_tire_types = $result['InstanceTypes']; 
                $types['paid'] = $free_tire_types;
            }
            /*
            $general_types      = "m1.small | m1.medium | m1.large | m1.xlarge | m3.medium | m3.large | m3.xlarge | m3.2xlarge | t1.micro";
            $coumpute_types     = "c1.medium | c1.xlarge | cc2.8xlarge | c3.large | c3.xlarge | c3.2xlarge | c3.4xlarge | c3.8xlarge";
            $memory_types       = "m2.xlarge | m2.2xlarge | m2.4xlarge | cr1.8xlarge | r3.large | r3.xlarge | r3.2xlarge | r3.4xlarge | r3.8xlarge";
            $storage_types      = "hs1.8xlarge | i2.xlarge | i2.2xlarge | i2.4xlarge | i2.8xlarge";
            $acclerated_types   = "g2.2xlarge | g2.8xlarge";
            $types['general_purpose']       = explode(" | ", $general_types);
            $types['compute_optimized']     = explode(" | ", $coumpute_types);
            $types['memory_optimized']      = explode(" | ", $memory_types);
            $types['storage_optimized']     = explode(" | ", $storage_types);
            $types['accelerated_computing'] = explode(" | ", $acclerated_types);*/
        }
        
        return $types;
    }
    
    /*
     * Function to get all regions
     */
    public function ec2_get_all_regions() {
        
        //Set initial value
        $regions  =array();
        //Get EC2 client
        $ec2Client = $this->ec2_get_client();
        
        if( $ec2Client ) {
            $result = $ec2Client->describeRegions();
            $statusCode = $result['@metadata']['statusCode'];
            //If API request successful
            if( $statusCode == 200 ) {
                $regions = $result['Regions'];                
            }
        }
        
        return $regions;
    }
    
    /*
     * Function to generate private key
     * 
     * @param $key_name string public key name
     * @param $region string AWS region
     * 
     * @return False|$saveKeyLocation string full path of private key
     */
    public function ec2_get_private_key( $key_name = "", $region = "us-west-1" ) {
        
        if( empty($key_name) ) {
            return false;
        }
        //Get EC2 client
        $ec2Client = $this->ec2_get_client( $region );
        if( !$ec2Client ) {
            return false;
        }
        
        $key_found = true;
        try{
            $result = $ec2Client->describeKeyPairs(array(
                'KeyNames' => array($key_name)
            ));            
        } catch (Exception $ex) {
            $key_found = false;
        }
        //if key not exists then create
        if( !$key_found ) {
            //Santize the key name to create a file name
            $key_name = sanitize_title($key_name);            
            // Create the key pair
            $result = $ec2Client->createKeyPair(array(
                'KeyName' => $key_name
            ));       
        }else{
            //if there is nothing to save in files then return
            return "";
        }
        
        //Varify if correct data returned
        if( $result['@metadata']['statusCode'] != 200 || empty($result['KeyName']) ) {
            return false;
        }
        
        // Save the private key
        $saveKeyLocation = EC2_PLUGIN_PATH . "keys/{$key_name}.pem";
        file_put_contents($saveKeyLocation, $result['keyMaterial']);

        // Update the key's permissions so it can be used with SSH
        chmod($saveKeyLocation, 0600);
        
        return $saveKeyLocation;
    }
    
    /*
     * EC2 function to change an instance state
     * 
     * @param $region string AWS region
     * @param $instance_id string AWS instance id
     * @param $state string AWS instance new state
     * 
     * @return FALSE|$result mixed
     */
    public function ec2_change_instance_state( $region = "", $instance_id = "", $state = "" ) {
        
        if( empty($instance_id) || empty($region)  || empty($state) ) {
            return false;
        }
        
        //Get EC2 client
        $ec2Client = $this->ec2_get_client( $region );
        
        if( $state == 'start' ) {
            $result = $ec2Client->startInstances(array(
                'InstanceIds' => array($instance_id),
            ));
        }elseif( $state == 'stop' ) {
            $result = $ec2Client->stopInstances(array(
                'InstanceIds' => array($instance_id),
            ));
        }elseif( $state == 'reboot' ) {
            $result = $ec2Client->rebootInstances(array(
                'InstanceIds' => array($instance_id),
            ));
        }elseif( $state == 'terminate' ) {
            $result = $ec2Client->terminateInstances(array(
                'InstanceIds' => array($instance_id),
            ));
        }
        
        if( $result['@metadata']['statusCode'] == 200 ) {
            return $result;
        }
        
        return false;
    }
    
    /*
     * Function to get all keys
     * 
     * @param $region string AWS region
     * 
     * @return $keys all getKeyPairs objects
     */
    public function ec2_get_key_pairs( $region = "" ) {
        
        //Get ALS client
        $ec2Client = $this->ec2_get_client( $region );
        if( !$ec2Client ) {
            return false;
        }
        
        $key_pairs = array();
        $result = $ec2Client->describeKeyPairs();
        
        //Varify if correct data returned
        if( $result['@metadata']['statusCode'] == 200 && is_array($result['KeyPairs']) ) {
            $key_pairs = $result['KeyPairs'];
        }
        
        return $key_pairs;
    }
    
    /*
     * EC2 function to get all instances
     * 
     * @param $region string AWS region
     * @return $instances array all instances of that region
     */
    public function ec2_get_all_instances( $region = "us-west-1", $param=array() ) {
        
        //Set initial value
        $instances = array();
        //Get EC2 client
        $ec2Client = $this->ec2_get_client( $region );
        
        if( $ec2Client ) {
            if( !empty($param) ) {
                $result = $ec2Client->describeInstances($param);
            }else{
                $result = $ec2Client->describeInstances();
            }
            $statusCode = $result['@metadata']['statusCode'];
            //If API request successful
            if( $statusCode == 200 ) {
                $reservations = $result['Reservations']; 
                if(is_array($reservations)) {
                    foreach( $reservations as $reservation ) {
                        if(is_array($reservation['Instances'])) {
                            foreach( $reservation['Instances'] as $instance ) {
                                $instances[] = $instance;
                            }
                        }
                    }
                }
            }
        }
        
        return $instances;
    }
    
    /*
     * Function to load the EC2 client
     * 
     * @param $region string AWS region
     * @return $ec2Client null|EC2 client
     */
    public function ec2_get_client( $region="" ) {
        
        global $ec2_options;
        $ec2Client = null;
        
        if( empty($region) ) {
            $region = "us-west-1";
        }
        
        //Check if config values are set
        if( !empty($ec2_options['aws_access_key_id']) && !empty($ec2_options['aws_secret_key']) ) {
            //Load the aws API
            require_once EC2_PLUGIN_PATH.'includes/aws/aws-autoloader.php';
            
            $ec2Client = Ec2Client::factory(array(
                        'region'        => $region,
                        'version'       => '2016-11-15',
                        'credentials' => array(
                            'key'    => $ec2_options['aws_access_key_id'],
                            'secret' => $ec2_options['aws_secret_key'],
                        )
                    ));
        }
        
        return $ec2Client;
    }
    
    /*
     * load front end or admin end html template
     * 
     * @param $template template name
     * @param $for either for front end and back end
     * @param $attr array of all parameters that we will pass to the template
     * 
     * return html of the template
     */
    public function ec2_load_template( $template='', $for='front', $attr=array() ) {
        
        global $ec2_options,$ec2_lang;
        
        do_action( 'ec2_before_load_template', $template, $for, $attr );
        
        $template = apply_filters( 'ec2_template_to_load', $template, $for, $attr );
        $attr = apply_filters( 'ec2_template_variables', $attr, $template, $for );
        
        if( empty($template) ) {
            return '';
        }
        if( is_array($attr) ) {
            extract($attr);
        }
        
        $html = '';
        $html = apply_filters( 'ec2_before_template_html', $html, $template, $for, $attr );
        
        ob_start();
        require EC2_PLUGIN_PATH.'templates/'.$for.'/'.$template.'.php';
        $html = ob_get_contents();
        ob_end_clean();  
        
        do_action( 'ec2_after_load_template', $template, $for, $attr, $html );
        
        $html = apply_filters( 'ec2_after_template_html', $html, $template, $for, $attr );
        
        return $html;
    }
    
    /*
     * load admin end messages/errors/warnings template
     * 
     * @param $message message string
     * @param $type either it is message? or error ? or warning?
     * 
     * return html of the template
     */
    public function ec2_get_message_html( $message, $type = 'message' ) {
        global $ec2_options,$ec2_lang;
        do_action( 'ec2_before_get_message_html', $message, $type );
        $message = apply_filters( 'ec2_message_text', $message, $type );
        $type = apply_filters( 'ec2_message_type', $type, $message );
        
        $html = '';
        
        $html = apply_filters( 'ec2_before_message_html', $html, $message, $type );
        
        $attr = array( 'message' => $message, 'type' => $type );
        
        $html = $this->ec2_load_template( $type, 'common', $attr );
        
        do_action( 'ec2_after_get_message_html', $message, $type );
        $html = apply_filters( 'ec2_after_message_html', $html, $message, $type );
        
        return $html;
    }
    
    /*
     * insert record into database
     * 
     * @param $table table in which we will insert record
     * @param $data array of data that we will insert
     * 
     * return last insert id
     */
    
    public function ec2_add_record( $table = '', $data = array() ) {
        
        if( empty($data) || empty($table) ) {
            return false;
        }
        
        global $wpdb;
        $exclude = array( 'btnsave' );
        $attr = "";
        $attr_val = "";
        foreach( $data as $k=>$val ) {            
            if(is_array($val)) {
                $val = maybe_serialize($val);
            }else{
                $val = $this->make_safe($val);
            }
            if( !in_array( $k, $exclude )) { 
                if( $attr == "" ) {
                    $attr.="`".$k."`";
                    $attr_val.="'".str_replace("'", "\\'",$val)."'";
                }else{
                    $attr.=", `".$k."`";
                    $attr_val.=", '".str_replace("'", "\\'",$val)."'";
                }                
            }
        }
        $sql = "INSERT INTO `".$wpdb->prefix.$table."` (".$attr.") VALUES (".$attr_val.")";
        $wpdb->query($sql);
        $lastid = $wpdb->insert_id;
        return $lastid;        
    }
    
    /*
     * insert multiple records into database
     * 
     * @param $table table in which we will insert record
     * @param $data array of data that we will insert
     * 
     * return last insert id
     */
    public function ec2_add_multiple_records( $table = '', $attr = array(), $data = array() ) {
        if( empty($data) || empty($table) || empty($attr) ) {
            return false;
        }
        global $wpdb;
        $exclude = array( 'btnsave' );
        $attr_str = "";
        foreach( $attr as $v ) {
            if( $attr_str == "" ) {
                $attr_str.="`".$v."`";
            }else{
                $attr_str.=", `".$v."`";
            }                
        }
        $attr_val = "";
        foreach( $data as $row ) { 
            if( $attr_val == '' ) {
                $attr_val.='(';
            }else{
                $attr_val.=',(';
            }
            $attr_val_row = '';
            foreach( $row as $k=>$val ) {
                if(is_array($val)) {
                    $val = maybe_serialize($val);
                }else{
                    $val = $this->make_safe($val);
                }
                if( !in_array( $k, $exclude )) {
                    if( $attr_val_row == "" ) {
                        $attr_val_row.="'".str_replace("'", "\\'",$val)."'";
                    }else{
                        $attr_val_row.=", '".str_replace("'", "\\'",$val)."'";
                    }                
                }
            }
            $attr_val.= $attr_val_row.')';
        }
        $sql = "INSERT INTO `".$wpdb->prefix.$table."` (".$attr_str.") VALUES ".$attr_val;
        $wpdb->query($sql);
        $lastid = $wpdb->insert_id;
        return $lastid;    
    }
    
    /*
     * update record into database
     * 
     * @param $table table for which we will update record
     * @param $data array of data that we will update
     * @param $where string for where clause of sql
     */
    public function ec2_update_record( $table = '', $data = array(), $where = '' ) {
        
        if( empty($where) || empty($data) || empty($table) ) {
            return false;
        }
        
        global $wpdb;
        $exclude = array( 'id','btnsave' );
        $attr = "";
        foreach( $data as $k=>$val ) {
            if(is_array($val)) {
                $val = maybe_serialize($val);
            }else{
                $val = $this->make_safe($val);
            }
            if( !in_array( $k, $exclude )) {
                if( $attr == "" ) {
                    $attr.="`".$k."` = '".str_replace("'", "\\'",$val)."'";                    
                }else{
                    $attr.=", `".$k."` = '".str_replace("'", "\\'",$val)."'";
                }                
            }
        }
        $sql = "UPDATE `".$wpdb->prefix.$table."` SET ".$attr." WHERE ".$where;
        $wpdb->query($sql);
        
        return true;
    }
    
    /*
     * delete record from database
     * 
     * @param $table table from which we will delete record
     * @param $where string for where clause of sql
     */
    public function ec2_del_record( $table = '', $where = '' ) {
        
        if( empty($where) || empty($table) ) {
            return false;
        }
        
        global $wpdb;
        $sql = "DELETE FROM `".$wpdb->prefix.$table."` WHERE ".$where;
        $wpdb->query($sql);
        return true;
    }
    
    /*
     * get data from the database table
     * 
     * @param $table database table from which we will get records
     * @param $where string for where clause of sql
     * @param $get_row return only one row or all rows
     * @param $attr string 
     * 
     * return a row or all rows objects
     */
    public function ec2_get_data( $table = '', $where = "1", $get_row = false, $attr = "*" ) {
        
        if( empty($table) ) {
            return false;
        }
        
        global $wpdb;
        
        $sql = "SELECT ".$attr." FROM `".$wpdb->prefix.$table."` WHERE ".$where;
        if( $get_row ) {
            $data = $wpdb->get_row($sql);
        }else{
            $data = $wpdb->get_results($sql);
        }
        
        return $data;
    }
    
    /*
     * make a variable snaitize and 
     * handel quotes double quotes and other characters 
     * 
     * @param $variable
     * 
     * return snaitizeed variable 
     */
    public function make_safe( $variable ) {

        $variable = sanitize_text_field($variable);
        $variable = esc_html($variable);
        
        return $variable;
    }
    
    public function array_sort($array, $on, $order='ASC'){

        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case 'ASC':
                    asort($sortable_array);
                    break;
                case 'DESC':
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }
    
    // Function to safe redirect the page without warnings
    public function redirect( $url ) {
        echo '<script language="javascript">window.location.href="'.$url.'";</script>';
        exit();
    }
    
    //Function will get called on plugin activation
    static function ec2_install() {

        do_action('ec2_before_install');

        require_once EC2_PLUGIN_PATH.'includes/install.php';

        do_action('ec2_after_install');
    }

    // Function will get called on plugin de activation
    static function ec2_uninstall() {

        do_action('ec2_before_uninstall');

        require_once EC2_PLUGIN_PATH.'includes/uninstall.php';

        do_action('ec2_after_uninstall');
    }
}

$ec2 = new EC2();