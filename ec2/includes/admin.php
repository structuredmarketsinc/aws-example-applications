<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Class that will hold functionality for admin side
 *
 * PHP version 5
 *
 * @category   Admin Side Code
 * @package    EC2 API
 * @author     Muhammad Atiq
 * @version    1.0.0
 * @since      File available since Release 1.0.0
*/

class EC2_Admin extends EC2
{
    //Admin side starting point. Will call appropriate admin side hooks
    public function __construct() {
        
        do_action('ec2_before_admin', $this );
        //All admin side code will go here
        
        add_action( 'admin_menu', array( $this, 'ec2_admin_menus' ) );    
        //Action hook to show instances
        add_action( 'wp_ajax_ec2_list_instances', array( $this, 'ec2_list_instances') );
        
        //Action hook to get AMIs options html
        add_action( 'wp_ajax_ec2_get_amis_html', array( $this, 'ec2_get_amis_html') );
        
        //Action hook to get instance types options html
        add_action( 'wp_ajax_ec2_get_instance_types_html', array( $this, 'ec2_get_instance_types_html') );
        
        //Action hook to get SSH key options html
        add_action( 'wp_ajax_ec2_get_ssh_html', array( $this, 'ec2_get_ssh_html') );
        
        do_action('ec2_after_admin', $this );            
    }
    
    /*
     * Function to show admin menu
     */
    public function ec2_admin_menus(){
        
        add_menu_page( EC2_PLUGIN_NAME, EC2_PLUGIN_NAME, 'manage_options', 'ec2_settings', array( $this, 'ec2_settings' ) );
        add_submenu_page( 'ec2_settings', EC2_PLUGIN_NAME.' Settings', 'Settings', 'manage_options', 'ec2_settings', array( $this, 'ec2_settings' ) );
        add_submenu_page( 'ec2_settings', EC2_PLUGIN_NAME.' Instances', 'Instances', 'manage_options', 'ec2_instances', array( $this, 'ec2_instances' ) );
    }    
    
    /*
     * Settings page of the plugin
     */
    public function ec2_settings() {
        ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
        global $ec2_options, $ec2_lang;
        
        do_action('ec2_before_settings', $this, $ec2_options );
        $message = '';
        if( isset($_POST['btnsave']) && $_POST['btnsave'] != "" ) {            
            $exclude = array('btnsave');
            $ec2_options = array();            
            foreach( $_POST as $k => $v ) {
                if( !in_array( $k, $exclude )) {
                    if(!is_array($v)) {
                        $val = $this->make_safe($v);
                    }else{
                        $val = $v;
                    }
                    $ec2_options[$k] = $val;
                }
            }            
            update_option( 'ec2_settings', $ec2_options );
            
            //Load success message template
            $message = $this->ec2_get_message_html( __( 'Settings saved successfully!', 'ec2' ), 'message' );
        }
        
        $attr = $ec2_options;
        $attr['message'] = $message;
        //Load settings page template
        $html = $this->ec2_load_template( "settings", "admin", $attr );
        
        do_action('ec2_after_settings', $this, $ec2_options );
        
        echo $html;
    }
    
    /*
     * Ajax function to get the Key Pairs
     */
    public function ec2_get_ssh_html() {
        
        $message = $html = "";
        $error = false;
        $region = filter_input( INPUT_POST, 'region' );
        if( empty($region) ) {
            $error = true;
            $message = __( 'No region provided', 'als' );
        }
        $html = '<option value="">'.__( 'Select Key', 'als' ).'</option>';
        if( !$error ) {
            $keys  = $this->ec2_get_key_pairs( $region );
            if(is_array($keys)) {
                foreach ( $keys as $key=>$val ) {
                    $html.='<option value="'.$val['KeyName'].'">'.$val['KeyName'].'</option>';
                }
            }
            $html.='<option value="ec2_create_new_ssh">'.__( 'Create New Key', 'als' ).'</option>';
        }
        
        $response = array( 'error' => $error, 'message' => $message, 'html' => $html );
        wp_send_json($response);
    }
    
    /*
     * Ajax function to get AMIs based on the region
     */
    public function ec2_get_amis_html() {
        
        $message = $html = "";
        $error = false;
        $region = filter_input( INPUT_POST, 'region' );
        if( empty($region) ) {
            $error = true;
            $message = __( 'No region provided', 'ec2' );
        }
        $html = '<option value="">'.__( 'Select AMI', 'ec2' ).'</option>';
        if( !$error ) {
            $amis  = $this->ec2_get_available_amis( $region );
            if(is_array($amis)) {
                foreach ( $amis as $ami ) {
                    $html.='<option value="'.$ami['ImageId'].'|'.$ami['RootDeviceName'].'">'.$ami['Description'].'</option>';
                }
            }
        }
        
        $response = array( 'error' => $error, 'message' => $message, 'html' => $html );
        wp_send_json($response);
    }
    
    /*
     * Ajax function to get Instance Types based on the region
     */
    public function ec2_get_instance_types_html() {
        
        $message = $html = "";
        $error = false;
        $region = filter_input( INPUT_POST, 'region' );
        if( empty($region) ) {
            $error = true;
            $message = __( 'No region provided', 'ec2' );
        }
        $html = '<option value="">'.__( 'Select Instance Type', 'ec2' ).'</option>';
        if( !$error ) {
            $types  = $this->ec2_get_instance_types( $region );
            if(is_array($types)) {
                foreach ( $types as $types_cat=>$types_vals ) {
                    $html.='<optgroup label="'. ucwords(str_replace("_"," ",$types_cat)) .'">';
                    foreach( $types_vals as $type ) {
                        //if($types_cat=='free_tire') {
                        $type = $type['InstanceType'];
                        //}
                        $html.='<option value="'.$type.'">'.$type.'</option>';
                    }
                }
            }
        }
        
        $response = array( 'error' => $error, 'message' => $message, 'html' => $html );
        wp_send_json($response);
    }
    
    /*
     * Admin side page to show instances
     */
    public function ec2_instances() {
        
        $template = $html = $message = '';
        $action = filter_input( INPUT_GET, 'action' );
        $id     = filter_input( INPUT_GET, 'id' );
        
        if( $action == 'edit' && is_numeric( $id ) ) {
            //do edit
        }elseif( $action == 'add' ) {
            if( isset($_POST['btnsave']) && $_POST['btnsave'] != "" ) {            
                $message = $this->ec2_save_instance();
            }
            $regions = $this->ec2_get_all_regions();
            $attr = array();
            $attr['regions'] = $regions;
            $template = "add_instance";  
        }else{
            if( $action == 'del' && is_numeric( $id ) ){
                $this->ec2_terminate_instance( $id );
            }elseif( $action == 'launch' && is_numeric( $id ) ){
                $message = $this->ec2_launch_instance( $id );
            }elseif( $action == 'start' && is_numeric( $id ) ){
                $message = $this->ec2_start_instance( $id );
            }elseif( $action == 'stop' && is_numeric( $id ) ){
                $message = $this->ec2_stop_instance( $id );
            }elseif( $action == 'reboot' && is_numeric( $id ) ){
                $message = $this->ec2_reboot_instance( $id );
            }elseif ($action == 'del' && $_REQUEST['ids'] != "" ){
                $ids = explode(",", $_REQUEST['ids']);
                if(is_array($ids)) {
                    foreach($ids as $id) {
                        if(is_numeric($id)) {
                            $this->ec2_terminate_instance( $id );
                        }
                    }
                }
            }
            wp_enqueue_script( 'ec2_jtable_js' );
            wp_enqueue_style( 'ec2_jtable_css' );
            $attr = array();
            $attr = $_REQUEST;
            $template = "instances";            
        }
        if( !empty($template) ) {
            $attr['message'] = $message;
            $html = $this->ec2_load_template( $template, "admin", $attr );
        }
        echo $html;
    }
    
    /*
     * Function to start an instance
     */
    public function ec2_start_instance( $id ) {
        
        if( !is_numeric($id) ) {
            //Load error message template
            $message = $this->ec2_get_message_html( __( 'You must provide instance id to start it!', 'ec2' ), 'error' );
            return $message;
        }
        
        $instance = $this->ec2_get_data("ec2_instances", "id = '".$id."'", true);
        try {   
            $result = $this->ec2_change_instance_state($instance->region, $instance->instance_id, 'start' );
            if( empty($result) ) {
                //Load error message template
                $message = $this->ec2_get_message_html( __( 'There is some error to start this instance, please try again!', 'ec2' ), 'error' );
                return $message;
            }else{
                //Load success message template
                $message = $this->ec2_get_message_html( __( 'Instance Start successfully!', 'ec2' ), 'message' );
            }
        } catch (Exception $ex) {
            //Load error message template
            $message = $this->ec2_get_message_html( __( $ex->getMessage(), 'ec2' ), 'error' );
            return $message;
        }
        
        return $message;
    }
    
    /*
     * Function to stop an instance
     * 
     * @param $id interger ID of the instance in local database
     * 
     * @return $message html of message/error
     */
    public function ec2_stop_instance( $id ) {
        
        if( !is_numeric($id) ) {
            //Load error message template
            $message = $this->ec2_get_message_html( __( 'You must provide instance id to stop it!', 'ec2' ), 'error' );
            return $message;
        }
        
        $instance = $this->ec2_get_data("ec2_instances", "id = '".$id."'", true);
        try {   
            $result = $this->ec2_change_instance_state($instance->region, $instance->instance_id, 'stop' );
            if( empty($result) ) {
                //Load error message template
                $message = $this->ec2_get_message_html( __( 'There is some error to stop this instance, please try again!', 'ec2' ), 'error' );
                return $message;
            }else{
                //Load success message template
                $message = $this->ec2_get_message_html( __( 'Instance Stoped successfully!', 'ec2' ), 'message' );
            }
        } catch (Exception $ex) {
            //Load error message template
            $message = $this->ec2_get_message_html( __( $ex->getMessage(), 'ec2' ), 'error' );
            return $message;
        }
        
        return $message;
    }
    
    /*
     * Function to stop an instance
     * 
     * @param $id interger ID of the instance in local database
     * 
     * @return $message html of message/error
     */
    public function ec2_reboot_instance( $id ) {
        
        if( !is_numeric($id) ) {
            //Load error message template
            $message = $this->ec2_get_message_html( __( 'You must provide instance id to reboot it!', 'ec2' ), 'error' );
            return $message;
        }
        
        $instance = $this->ec2_get_data("ec2_instances", "id = '".$id."'", true);
        try {   
            $result = $this->ec2_change_instance_state($instance->region, $instance->instance_id, 'reboot' );
            if( empty($result) ) {
                //Load error message template
                $message = $this->ec2_get_message_html( __( 'There is some error to reboot this instance, please try again!', 'ec2' ), 'error' );
                return $message;
            }else{
                //Load success message template
                $message = $this->ec2_get_message_html( __( 'Instance reboot successfully!', 'ec2' ), 'message' );
            }
        } catch (Exception $ex) {
            //Load error message template
            $message = $this->ec2_get_message_html( __( $ex->getMessage(), 'ec2' ), 'error' );
            return $message;
        }
        
        return $message;
    }
    
    /*
     * Function to terminate an instance
     * 
     * @param $id interger ID of the instance in local database
     * 
     * @return $message html of message/error
     */
    public function ec2_terminate_instance( $id ) {
        
        if( !is_numeric($id) ) {
            //Load error message template
            $message = $this->ec2_get_message_html( __( 'You must provide instance id to terminate it!', 'ec2' ), 'error' );
            return $message;
        }
        
        $instance = $this->ec2_get_data("ec2_instances", "id = '".$id."'", true);
        try {   
            $result = $this->ec2_change_instance_state($instance->region, $instance->instance_id, 'terminate' );
            if( empty($result) ) {
                //Load error message template
                $message = $this->ec2_get_message_html( __( 'There is some error to terminate this instance, please try again!', 'ec2' ), 'error' );
                return $message;
            }else{
                $this->ec2_del_record("ec2_instances", "id = '".$id."'");
                //Load success message template
                $message = $this->ec2_get_message_html( __( 'Instance terminated successfully!', 'ec2' ), 'message' );
            }
        } catch (Exception $ex) {
            //Load error message template
            $message = $this->ec2_get_message_html( __( $ex->getMessage(), 'ec2' ), 'error' );
            return $message;
        }
        
        return $message;
    }
    
    /*
     * Function to launch an existing instance or create new instance
     * 
     * @param $id interger ID of the instance in local database
     * 
     * @return $message html of message/error
     */
    public function ec2_launch_instance( $id ) {
        
        if( !is_numeric($id) ) {
            //Load error message template
            $message = $this->ec2_get_message_html( __( 'You must provide instance id to launch it!', 'ec2' ), 'error' );
            return $message;
        }
        $instance_id = "";
        
        try {        
            $instance = $this->ec2_get_data("ec2_instances", "id = '".$id."'", true);
            if( $instance ) {
                if( !empty($instance->instance_id) ) {
                    $ec2_instances = $this->ec2_get_all_instances($instance->region, array("InstanceIds"=>array($instance->instance_id)));
                    if( !empty($ec2_instances) ) {
                        $ec2_instance = $ec2_instances[0];
                        $instance_id = $ec2_instance['InstanceId'];
                    }
                }

                if( empty($instance_id) ) {
                    $ec2_instance = $this->ec2_create_instance( $instance->region, $instance->ssh, $instance->instance_type, $instance->size, $instance->ami, $instance->ami_device_name );
                    if( !empty($ec2_instance) ) {                    
                        $instance_id = $ec2_instance['InstanceId'];
                        $this->ec2_update_record( "ec2_instances", array( "private_key_path" => $ec2_instance['KeyPath'], "instance_id" => $instance_id ), "id = '".$id."'" );
                    }
                }
            }

            if( empty($instance_id) ) {
                //Load error message template
                $message = $this->ec2_get_message_html( __( 'There is some error to launch the instance', 'ec2' ), 'error' );
                return $message;
            }else{
                //Load success message template
                $message = $this->ec2_get_message_html( __( 'Instance launched successfully!', 'ec2' ), 'message' );
            }
        }catch (Exception $ex) {
            //Load error message template
            $message = $this->ec2_get_message_html( __( $ex->getMessage(), 'ec2' ), 'error' );
            return $message;
        }
        
        return $message;
    }
    
    /*
     * Function to save instance data in database
     * 
     * @param $id interger ID of the instance in local database
     * 
     * @return $message html of message/error
     */
    public function ec2_save_instance( $id = 0 ) {
        
        $message = '';
        $exclude    = array('btnsave');
        $data       = $_POST;
        $ami        = filter_input( INPUT_POST, 'ami' );
        $ssh_new    = filter_input( INPUT_POST, 'ssh_new' );
        $ami_arr    = explode( "|", $ami );
        $data['ami']= trim($ami_arr[0]);
        $data['ami_device_name'] = trim($ami_arr[1]);
        
        if( !empty($ssh_new) && $data['ssh'] == "ec2_create_new_ssh" ) {
            $data['ssh']   = $ssh_new;            
        }
        $data['ssh'] = sanitize_title($data['ssh']); 
        unset( $data['ssh_new']);
        
        if( !$id && $this->ec2_add_record( "ec2_instances", $data ) ) {
            //Load success message template
            $message = $this->ec2_get_message_html( __( 'Instance saved successfully!', 'ec2' ), 'message' );
        }elseif( $id && $this->ec2_update_record("ec2_instances", $data, "id = '".$id."'") ) {
            //Load success message template
            $message = $this->ec2_get_message_html( __( 'Instance updated successfully!', 'ec2' ), 'message' );
        }else{
            //Load error message template
            $message = $this->ec2_get_message_html( __( 'There is some error to save the instance!', 'ec2' ), 'error' );
        }
        
        return $message;
    }
    
    /*
     * Ajax callback function to load instances
     */
    public function ec2_list_instances() {
        
        $sorting    = filter_input( INPUT_GET, 'jtSorting' );
        $start      = filter_input( INPUT_GET, 'jtStartIndex' );
        $page_size  = filter_input( INPUT_GET, 'jtPageSize' );
        $end        = $start+$page_size;
        
        $records = $this->ec2_get_data( "ec2_instances" ); 
        
        $recordCount = sizeof($records);
        if( $sorting != "" ) {
            $sorting_arr = explode(" ",$sorting);
            $orderby = trim($sorting_arr[0]);
            $order = trim($sorting_arr[1]);
        }
        
        $rows = $final_rows = array();
        $index = 0;
        foreach ( $records as $row ) {
            $key = $state = "";
            $ec2_instances = null;
            try { 
                $ec2_instances = $this->ec2_get_all_instances($row->region, array("InstanceIds"=>array($row->instance_id)));                
            }catch (Exception $ex) {
                
            }
            if( !empty($ec2_instances) ) {
                $ec2_instance = $ec2_instances[0];
                $state = $ec2_instance['State']['Name'];//terminated|running|pending|rebooting|stopped|shutting-down|stopping|terminating
                $key = $ec2_instance['KeyName'];
            }
            if($state == 'terminated' || $state == 'terminating') {
                $this->ec2_del_record("ec2_instances", "id = '".$row->id."'");
            }else{
                $actions = '';
                if( $state == '' ) {
                    $actions.='<td><a class="ec2_actions '.$state.'" title="Launch" href="admin.php?page=ec2_instances&action=launch&id='.$row->id.'">'.__( 'Launch', 'ec2' ).'</a></td>';
                }
                if( $state == 'running' || $state == 'pending' ) {
                    $actions.='<td><a class="ec2_actions '.$state.'" title="Stop" href="admin.php?page=ec2_instances&action=stop&id='.$row->id.'" onclick="javascript: return confirm(\'Are you sure you want to stop it?\');">'.__( 'Stop', 'ec2' ).'</a></td>';
                }
                if( $state == 'stopped' || $state == 'stopping' ) {
                    $actions.='<td><a class="ec2_actions '.$state.'" title="Start" href="admin.php?page=ec2_instances&action=start&id='.$row->id.'">'.__( 'Start', 'ec2' ).'</a></td>';
                }
                if( $state != '' && $state != 'terminated' && $state != 'terminating' ) {                    
                    $actions.='<td><a class="ec2_actions terminate" title="Reboot" href="admin.php?page=ec2_instances&action=reboot&id='.$row->id.'" onclick="javascript: return confirm(\'Are you sure you want to reboot it?\');">'.__( 'Reboot', 'ec2' ).'</a></td>';
                    $actions.='<td><a class="ec2_actions terminate" title="Terminate" href="admin.php?page=ec2_instances&action=del&id='.$row->id.'" onclick="javascript: return confirm(\'Are you sure you want to terminate it?\');">'.__( 'Terminate', 'ec2' ).'</a></td>';
                }
                $record = array();
                $record['Action']       = '<table class="ec2_actions"><tr>'.$actions.'</tr></table>';
                $record['ID']           = $row->id;
                $record['Region']       = $row->region;
                $record['Size']         = $row->size.'GB';
                $record['SSH']          = $row->ssh;
                $record['InstanceType'] = $row->instance_type;
                $record['State']        = strtoupper($state);
                $rows[] = $record;
            }
        }
        
        $rows = $this->array_sort($rows, $orderby,$order);
        foreach ( $rows as $row ) {
            if($index >= $start && $index <= $end) {
                $final_rows[] = $row;
            }
            $index++;
        }
     
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = $recordCount;
        $jTableResult['Records'] = $final_rows;
        print json_encode($jTableResult);
        exit();
    }
}

$ec2_admin = new EC2_Admin();