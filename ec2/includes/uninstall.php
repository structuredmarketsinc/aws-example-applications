<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Class that will hold functionality for plugin deactivation
 *
 * PHP version 5
 *
 * @category   Uninstall
 * @package    EC2 API
 * @author     Muhammad Atiq
 * @version    1.0.0
 * @since      File available since Release 1.0.0
*/

class EC2_Uninstall extends EC2
{
    public function __construct() {
        
        do_action('ec2_before_uninstall', $this );
        
        do_action('ec2_after_uninstall', $this );
    }    
}

$ec2_uninstall = new EC2_Uninstall();