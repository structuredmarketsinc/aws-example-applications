<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Class that will hold functionality for plugin activation
 *
 * PHP version 5
 *
 * @category   Install
 * @package    EC2 API
 * @author     Muhammad Atiq
 * @version    1.0.0
 * @since      File available since Release 1.0.0
*/

class EC2_Install extends EC2
{
    public function __construct() {
        
        do_action('ec2_before_install', $this );
        
        global $wpdb;
        
        $sql="CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."ec2_instances` (
                `id` int(11) NOT NULL auto_increment,
                `region` varchar(250) DEFAULT NULL,
                `size` varchar(250) DEFAULT NULL,
                `ssh` varchar(250) DEFAULT NULL,                
                `instance_id` varchar(250) DEFAULT NULL,                
                `ami` varchar(250) DEFAULT NULL,    
                `ami_device_name` varchar(250) DEFAULT NULL,    
                `instance_type` varchar(250) DEFAULT NULL,    
                `private_key_path` text NULL,
                PRIMARY KEY  (`id`)
              ) AUTO_INCREMENT=1 ;";

        $wpdb->query($sql);
        
        do_action('ec2_after_install', $this );
    }    
}

$ec2_install = new EC2_Install();