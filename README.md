# README #

Contains code for two plugins to create servers on AWS for EC2 and LIGHTSAIL.

### What is this repository for? ###

This project contains code for two plugins to create servers on AWS for EC2 and LIGHTSAIL.
The plugins were created as samples to test the AWS APIs for EC2 and LIGHTSAIL.

### How do I get set up? ###

There are two folders in this project:
- EC2
- LIGHTSAIL

The EC2 folder is a WordPress plugin that allows you to create servers on the AWS EC2 service.
The LIGHTSAIL folder a WordPress plugin that allows you to create servers on the AWS LIGHTSAIL service.

To install either of them.
1. Zip up the respective folder (eg: zip up the EC2 folder or the LIGHTSAIL folder)
2. Upload to your wordpress server using the PLUGINS screen.
3. Activate.

You can create servers, stop them, reboot and delete them.

### Who do I talk to? ###

These two plugins were created in order for us to understand how the AWS API works.  There is no support for it.
However, you can see where this project lead by going to the https://wpclouddeploy.com website.
