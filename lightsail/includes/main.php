<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Plugin main class that will control the whole skeleton and common functions
 *
 * PHP version 5
 *
 * @category   Main
 * @package    Lightsail API
 * @author     Muhammad Atiq
 * @version    1.0.0
 * @since      File available since Release 1.0.0
*/

use Aws\Lightsail\LightsailClient;

class ALS
{
     
    //Plugin starting point. Will call appropriate actions
    public function __construct() {

        add_action( 'plugins_loaded', array( $this, 'als_init' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'als_enqueue_scripts' ), 10 );
        add_action( 'admin_enqueue_scripts', array( $this, 'als_enqueue_scripts' ), 10 );
    }

    //Plugin initialization
    public function als_init() {

        do_action('als_before_init');
        
        global $als_options,$als_lang;
        
        $als_options = get_option( 'als_settings' );
        load_plugin_textdomain( 'als', FALSE, ALS_LANG_DIR );        
        
        if(is_admin()){            
            require_once ALS_PLUGIN_PATH.'includes/admin.php';            
        }

        do_action('als_after_init');
    }
    
    //Function will add CSS and JS files
    public function als_enqueue_scripts() {
        
        do_action('als_before_enqueue_scripts');
        
        wp_enqueue_script( 'jquery' );
        wp_register_script( 'als_jtable_js', ALS_PLUGIN_URL. "assets/js/jtable/jquery.jtable.js", array( 'jquery-ui-widget', 'jquery-ui-mouse', 'jquery-ui-position', 'jquery-ui-accordion', 'jquery-ui-autocomplete', 'jquery-ui-button', 'jquery-ui-datepicker', 'jquery-ui-dialog', 'jquery-ui-draggable', 'jquery-ui-droppable', 'jquery-effects-blind', 'jquery-effects-bounce', 'jquery-effects-clip', 'jquery-effects-drop', 'jquery-effects-explode', 'jquery-effects-fade', 'jquery-effects-fold', 'jquery-effects-highlight', 'jquery-effects-pulsate', 'jquery-effects-scale', 'jquery-effects-shake', 'jquery-effects-slide', 'jquery-effects-transfer', 'jquery-ui-menu', 'jquery-ui-progressbar', 'jquery-ui-resizable', 'jquery-ui-selectable', 'jquery-ui-slider', 'jquery-ui-sortable', 'jquery-ui-spinner', 'jquery-ui-tabs', 'jquery-ui-tooltip' ), time() );
        wp_register_style( 'als_jtable_css', ALS_PLUGIN_URL. "assets/js/jtable/themes/lightcolor/gray/jtable.css" );        
        wp_enqueue_script( 'als_js', ALS_PLUGIN_URL.'assets/js/javascript.js', array( 'jquery' ), time() );
        wp_enqueue_style( 'als_css', ALS_PLUGIN_URL.'assets/css/style.css', array(), time() );
        
        wp_localize_script( 'als_js', 'als_data', array(
                            'ajaxurl' => admin_url( 'admin-ajax.php' )
                            ));
        
        do_action('als_after_enqueue_scripts');
    }
    
    /*
     * Function to create an instance
     * 
     * @param $region string AWS region e.g. us-west-2
     * @param $key_name string public key name that should be linked e.g. TestKey
     * @param $instance_type string instance type e.g. t2.micro, m1.micro etc instance type should be from the region specified
     * @param $disk_size interger disk size in GBs. e.g. 80
     * @param $image_id string AMI image ID e.g. ami-0e01ce4ee18447327 AMI should belongs to the region specified
     * @param $device_name string Device name, Device name should be as provided in the image AMI
     * 
     * @return False|$instance AWS instance object
     */
    public function als_create_instance( $region="", $zone="", $blueprintId = "", $bundleId="micro_1_0", $instance_name="", $key_name="", $create_key = true  ) {
        
        if( empty($zone) || empty($blueprintId) || empty($bundleId) || empty($instance_name) ) {
            return false;
        }
        
        //Get ALS client
        $alsClient = $this->als_get_client( $region );
        if( !$alsClient ) {
            return false;
        }
        
        $instance = array();
        
        $params['availabilityZone'] = $zone;
        $params['blueprintId']      = $blueprintId;
        $params['bundleId']         = $bundleId;
        $params['instanceNames']    = array( $instance_name );
        
        if( !empty($key_name) ) {
            //Santize the key name to create a file name
            $key_name = sanitize_title($key_name);
            $key_paths    = $this->als_create_key_pairs( $key_name, $region );            
            $params['keyPairName']      = $key_name;
        }
        
        //Create Instance
        $result = $alsClient->createInstances($params);
        
        $statusCode = $result['@metadata']['statusCode'];
        if( $statusCode == 200 && is_array($result['operations']) ) {
            $instance                       = $result['operations'][0];
            if( $create_key ) {
                $instance['private_key_path']   = $key_paths['private'];
                $instance['public_key_path']    = $key_paths['public'];
            }
        }
        
        return $instance;
    }
    
    /*
     * Function to generate private key
     * 
     * @param $key_name string public key name
     * @param $region string AWS region
     * 
     * @return False|$saveKeyLocation string full path of private key
     */
    public function als_create_key_pairs( $key_name = "", $region = "us-west-2" ) {
        
        if( empty($key_name) ) {
            return false;
        }
        //Get ALS client
        $alsClient = $this->als_get_client( $region );
        if( !$alsClient ) {
            return false;
        }
        
        $key_found = true;
        try{
            $result = $alsClient->getKeyPair(array(
                'keyPairName' => $key_name
            ));
        } catch (Exception $ex) {
            $key_found = false;
        }
        
        //if key not exists then create
        if( !$key_found ) {
            //Santize the key name to create a file name
            $key_name = sanitize_title($key_name);
            
            // Create the key pair
            $result = $alsClient->createKeyPair(array(
                'keyPairName' => $key_name
            ));        
        }else{
            //if there is nothing to save in files then return
            return array( 'private' => "", 'public' => "" );
        }
        
        //Varify if correct data returned
        if( $result['@metadata']['statusCode'] != 200 || empty($result['keyPair']) ) {
            return false;
        }
        
        // Save the private key
        $savePrivateKeyLocation = EC2_PLUGIN_PATH . "keys/".$key_name."_private.key";
        file_put_contents($savePrivateKeyLocation, $result['privateKeyBase64']);
        
        // Save the public key
        $savePublicKeyLocation = EC2_PLUGIN_PATH . "keys/".$key_name."_public.key";
        file_put_contents($savePublicKeyLocation, $result['publicKeyBase64']);
        
        // Update the key's permissions so it can be used with SSH
        chmod($savePrivateKeyLocation, 0600);
        chmod($savePublicKeyLocation, 0600);
        
        return array( 'private' => $savePrivateKeyLocation, 'public' => $savePublicKeyLocation );
    }
    
    /*
     * Function to get all keys
     * 
     * @param $region string AWS region
     * 
     * @return $keys all getKeyPairs objects
     */
    public function als_get_key_pairs( $region = "" ) {
        
        //Get ALS client
        $alsClient = $this->als_get_client( $region );
        if( !$alsClient ) {
            return false;
        }
        
        $key_pairs = array();
        $result = $alsClient->getKeyPairs();
        //Varify if correct data returned
        if( $result['@metadata']['statusCode'] == 200 && is_array($result['keyPairs']) ) {
            $key_pairs = $result['keyPairs'];
        }
        
        return $key_pairs;
    }
    
    /*
     * Function to get all Images
     * 
     * @param $region string AWS region
     * @param $limit integer number of AMI to get
     * 
     * @return $images all AMIs objects
     */
    public function als_get_blueprints( $region = "" ) {
        
        if( empty($region) ) {
            return false;
        }
        
        //Set initial value
        $blueprints  =array();
        //Get ALS client
        $alsClient = $this->als_get_client( $region );
        
        if( $alsClient ) {
            $result = $alsClient->getBlueprints(array( "includeInactive" => false ));
            $statusCode = $result['@metadata']['statusCode'];
            if( $statusCode == 200 && is_array($result['blueprints']) ) {
                foreach( $result['blueprints'] as $blueprint ) {
                    $blueprintId = $blueprint['blueprintId'];
                    $name        = $blueprint['name'];
                    $platform    = $blueprint['platform'];
                    $blueprints[$blueprintId] = array( 'name' => $name, 'platform' => $platform );
                }
            }
        }
        
        return $blueprints;
    }
    
    /*
     * Function to get all instance types
     * 
     * @param $region string AWS region
     * 
     * @return $types array of all instance types
     */
    public function als_get_bundles( $region = "", $platform="" ) {
        
        //Set initial value
        $bundles  =array();
        //Get ALS client
        $alsClient = $this->als_get_client( $region );
        
        if( $alsClient ) {
            $result = $alsClient->getBundles(array( "includeInactive" => false ) );
            $statusCode = $result['@metadata']['statusCode'];
            //If API request successful
            if( $statusCode == 200 ) {
                $bundles_data = $result['bundles']; 
                foreach( $bundles_data as $bundle ) {
                    $bundleId   = $bundle['bundleId'];
                    $name       = $bundle['name'];                    
                    if(empty($platform)){
                        $bundles[$bundleId] = $name;
                    }elseif( !empty($platform) && in_array($platform, $bundle['supportedPlatforms']) ) {
                        $bundles[$bundleId] = $name;
                    }
                }
            }
        }
        
        return $bundles;
    }
    
    /*
     * Function to get all regions and their zones
     * 
     * @return $regions_with_zones array AWS regions with zones
     */
    public function als_get_all_regions_with_zones() {
        
        //Set initial value
        $regions_with_zones = array();
        
        //Check if saved in cache
        $regions_with_zones = get_transient( 'als_regions_with_zones' );
        if( $regions_with_zones ) {
            return $regions_with_zones;
        }
        
        //Get regions
        $regions = $this->als_get_all_regions();
        if( is_array($regions) ) {
            foreach( $regions as $region ) {
                $zones = $this->als_get_zones( $region['name'] );
                if(is_array($zones)) {
                    $region_name = $region['name'];
                    $regions_with_zones[$region_name] = $zones;                    
                }
            }
            //Save in transient
            set_transient( 'als_regions_with_zones', $regions_with_zones, DAY_IN_SECONDS );
        }
        
        return $regions_with_zones;
    }
    
    /*
     * Function to get all zones of region
     * 
     * @param $region string AWS region
     * 
     * @return $zones array of AWS region zones
     */
    public function als_get_zones( $region = "" ) {
        
        //Set initial value
        $zones = array();
        //Get ALS client
        $alsClient = $this->als_get_client( $region ); 
        
        if( $alsClient ) {
            $result = $alsClient->getRegions(array( "includeAvailabilityZones" => true, "includeRelationalDatabaseAvailabilityZones" => true ));
            $statusCode = $result['@metadata']['statusCode'];
            //If API request successful
            if( $statusCode == 200 ) {
                $regions_data = $result['regions']; 
                foreach( $regions_data as $region ) {
                    if( is_array($region['availabilityZones']) && sizeof($region['availabilityZones']) > 0 ) {
                        $displayName        = $region['displayName'];
                        $availabilityZones  = $region['availabilityZones'];
                        foreach( $availabilityZones as $zone ) {                            
                            $zoneName           = $zone['zoneName'];
                            $zones[$zoneName]   = $displayName." - ".$zoneName;
                        }
                    }
                }   
            }
        }
        
        return $zones;
    }
    
    /*
     * Function to get all zones
     * 
     * @return $regions array AWS regions
     */
    public function als_get_all_regions() {
        
        //Set initial value
        $regions  = array();
        //Get ALS client
        $alsClient = $this->als_get_client();
        
        if( $alsClient ) {
            
            $result = $alsClient->getRegions();
            $statusCode = $result['@metadata']['statusCode'];
            //If API request successful
            if( $statusCode == 200 ) {
                $regions = $result['regions'];
            }
        }
        
        return $regions;
    }
    
    /*
     * ALS function to change an instance state
     * 
     * @param $region string AWS region
     * @param $instance_id string AWS instance id
     * @param $state string AWS instance new state
     * 
     * @return FALSE|$result mixed
     */
    public function als_change_instance_state( $region = "", $instance_name = "", $state = "" ) {
        
        if( empty($instance_name) || empty($region)  || empty($state) ) {
            return false;
        }
        
        //Get ALS client
        $alsClient = $this->als_get_client( $region );
        
        if( $state == 'start' ) {
            $result = $alsClient->startInstance(array(
                'instanceName' => $instance_name
            ));
        }elseif( $state == 'stop' ) {
            $result = $alsClient->stopInstance(array(
                'instanceName' => $instance_name,
                'force'        => true
            ));
        }elseif( $state == 'reboot' ) {
            $result = $alsClient->rebootInstance(array(
                'instanceName' => $instance_name
            ));
        }elseif( $state == 'terminate' ) {
            $result = $alsClient->deleteInstance(array(
                'instanceName'      => $instance_name,
                'forceDeleteAddOns' => true
            ));
        }
        
        if( $result['@metadata']['statusCode'] == 200 ) {
            return $result;
        }
        
        return false;
    }
    
    /*
     * ALS function to get all instances
     * 
     * @param $region string AWS region
     * @return $instances array all instances of that region
     */
    public function als_get_instance( $region = "us-west-2", $instance_name="" ) {
        
        if( empty($instance_name) ) {
            return false;
        }
        
        //Set initial value
        $instance = array();
        //Get ALS client
        $alsClient = $this->als_get_client( $region );
        
        if( $alsClient ) {
            $result = $alsClient->getInstance( array( "instanceName" => $instance_name ) );
            $statusCode = $result['@metadata']['statusCode'];
            //If API request successful
            if( $statusCode == 200 ) {
                $instance = $result['instance'];                
            }
        }
        
        return $instance; 
    }
    
    /*
     * Function to load the ALS client
     * 
     * @param $region string AWS region
     * @return $alsClient null|ALS client
     */
    public function als_get_client( $region="" ) {
        
        global $als_options;
        $alsClient = null;
        
        if( empty($region) ) {
            $region = "us-west-2";
        }
        
        //Check if config values are set
        if( !empty($als_options['aws_access_key_id']) && !empty($als_options['aws_secret_key']) ) {
            //Load the aws API
            require_once ALS_PLUGIN_PATH.'includes/aws/aws-autoloader.php';
            
            $alsClient = LightsailClient::factory(array(
                        'region'        => $region,
                        'version'       => '2016-11-28',
                        'credentials' => array(
                            'key'    => $als_options['aws_access_key_id'],
                            'secret' => $als_options['aws_secret_key'],
                        )
                    ));
        }
        
        return $alsClient;
    }
    
    /*
     * load front end or admin end html template
     * 
     * @param $template template name
     * @param $for either for front end and back end
     * @param $attr array of all parameters that we will pass to the template
     * 
     * return html of the template
     */
    public function als_load_template( $template='', $for='front', $attr=array() ) {
        
        global $als_options,$als_lang;
        
        do_action( 'als_before_load_template', $template, $for, $attr );
        
        $template = apply_filters( 'als_template_to_load', $template, $for, $attr );
        $attr = apply_filters( 'als_template_variables', $attr, $template, $for );
        
        if( empty($template) ) {
            return '';
        }
        if( is_array($attr) ) {
            extract($attr);
        }
        
        $html = '';
        $html = apply_filters( 'als_before_template_html', $html, $template, $for, $attr );
        
        ob_start();
        require ALS_PLUGIN_PATH.'templates/'.$for.'/'.$template.'.php';
        $html = ob_get_contents();
        ob_end_clean();  
        
        do_action( 'als_after_load_template', $template, $for, $attr, $html );
        
        $html = apply_filters( 'als_after_template_html', $html, $template, $for, $attr );
        
        return $html;
    }
    
    /*
     * load admin end messages/errors/warnings template
     * 
     * @param $message message string
     * @param $type either it is message? or error ? or warning?
     * 
     * return html of the template
     */
    public function als_get_message_html( $message, $type = 'message' ) {
        global $als_options,$als_lang;
        do_action( 'als_before_get_message_html', $message, $type );
        $message = apply_filters( 'als_message_text', $message, $type );
        $type = apply_filters( 'als_message_type', $type, $message );
        
        $html = '';
        
        $html = apply_filters( 'als_before_message_html', $html, $message, $type );
        
        $attr = array( 'message' => $message, 'type' => $type );
        
        $html = $this->als_load_template( $type, 'common', $attr );
        
        do_action( 'als_after_get_message_html', $message, $type );
        $html = apply_filters( 'als_after_message_html', $html, $message, $type );
        
        return $html;
    }
    
    /*
     * insert record into database
     * 
     * @param $table table in which we will insert record
     * @param $data array of data that we will insert
     * 
     * return last insert id
     */
    
    public function als_add_record( $table = '', $data = array() ) {
        
        if( empty($data) || empty($table) ) {
            return false;
        }
        
        global $wpdb;
        $exclude = array( 'btnsave' );
        $attr = "";
        $attr_val = "";
        foreach( $data as $k=>$val ) {            
            if(is_array($val)) {
                $val = maybe_serialize($val);
            }else{
                $val = $this->make_safe($val);
            }
            if( !in_array( $k, $exclude )) { 
                if( $attr == "" ) {
                    $attr.="`".$k."`";
                    $attr_val.="'".str_replace("'", "\\'",$val)."'";
                }else{
                    $attr.=", `".$k."`";
                    $attr_val.=", '".str_replace("'", "\\'",$val)."'";
                }                
            }
        }
        $sql = "INSERT INTO `".$wpdb->prefix.$table."` (".$attr.") VALUES (".$attr_val.")";
        $wpdb->query($sql);
        $lastid = $wpdb->insert_id;
        return $lastid;        
    }
    
    /*
     * insert multiple records into database
     * 
     * @param $table table in which we will insert record
     * @param $data array of data that we will insert
     * 
     * return last insert id
     */
    public function als_add_multiple_records( $table = '', $attr = array(), $data = array() ) {
        if( empty($data) || empty($table) || empty($attr) ) {
            return false;
        }
        global $wpdb;
        $exclude = array( 'btnsave' );
        $attr_str = "";
        foreach( $attr as $v ) {
            if( $attr_str == "" ) {
                $attr_str.="`".$v."`";
            }else{
                $attr_str.=", `".$v."`";
            }                
        }
        $attr_val = "";
        foreach( $data as $row ) { 
            if( $attr_val == '' ) {
                $attr_val.='(';
            }else{
                $attr_val.=',(';
            }
            $attr_val_row = '';
            foreach( $row as $k=>$val ) {
                if(is_array($val)) {
                    $val = maybe_serialize($val);
                }else{
                    $val = $this->make_safe($val);
                }
                if( !in_array( $k, $exclude )) {
                    if( $attr_val_row == "" ) {
                        $attr_val_row.="'".str_replace("'", "\\'",$val)."'";
                    }else{
                        $attr_val_row.=", '".str_replace("'", "\\'",$val)."'";
                    }                
                }
            }
            $attr_val.= $attr_val_row.')';
        }
        $sql = "INSERT INTO `".$wpdb->prefix.$table."` (".$attr_str.") VALUES ".$attr_val;
        $wpdb->query($sql);
        $lastid = $wpdb->insert_id;
        return $lastid;    
    }
    
    /*
     * update record into database
     * 
     * @param $table table for which we will update record
     * @param $data array of data that we will update
     * @param $where string for where clause of sql
     */
    public function als_update_record( $table = '', $data = array(), $where = '' ) {
        
        if( empty($where) || empty($data) || empty($table) ) {
            return false;
        }
        
        global $wpdb;
        $exclude = array( 'id','btnsave' );
        $attr = "";
        foreach( $data as $k=>$val ) {
            if(is_array($val)) {
                $val = maybe_serialize($val);
            }else{
                $val = $this->make_safe($val);
            }
            if( !in_array( $k, $exclude )) {
                if( $attr == "" ) {
                    $attr.="`".$k."` = '".str_replace("'", "\\'",$val)."'";                    
                }else{
                    $attr.=", `".$k."` = '".str_replace("'", "\\'",$val)."'";
                }                
            }
        }
        $sql = "UPDATE `".$wpdb->prefix.$table."` SET ".$attr." WHERE ".$where;
        $wpdb->query($sql);
        
        return true;
    }
    
    /*
     * delete record from database
     * 
     * @param $table table from which we will delete record
     * @param $where string for where clause of sql
     */
    public function als_del_record( $table = '', $where = '' ) {
        
        if( empty($where) || empty($table) ) {
            return false;
        }
        
        global $wpdb;
        $sql = "DELETE FROM `".$wpdb->prefix.$table."` WHERE ".$where;
        $wpdb->query($sql);
        return true;
    }
    
    /*
     * get data from the database table
     * 
     * @param $table database table from which we will get records
     * @param $where string for where clause of sql
     * @param $get_row return only one row or all rows
     * @param $attr string 
     * 
     * return a row or all rows objects
     */
    public function als_get_data( $table = '', $where = "1", $get_row = false, $attr = "*" ) {
        
        if( empty($table) ) {
            return false;
        }
        
        global $wpdb;
        
        $sql = "SELECT ".$attr." FROM `".$wpdb->prefix.$table."` WHERE ".$where;
        if( $get_row ) {
            $data = $wpdb->get_row($sql);
        }else{
            $data = $wpdb->get_results($sql);
        }
        
        return $data;
    }
    
    /*
     * make a variable snaitize and 
     * handel quotes double quotes and other characters 
     * 
     * @param $variable
     * 
     * return snaitizeed variable 
     */
    public function make_safe( $variable ) {

        $variable = sanitize_text_field($variable);
        $variable = esc_html($variable);
        
        return $variable;
    }
    
    public function array_sort($array, $on, $order='ASC'){

        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case 'ASC':
                    asort($sortable_array);
                    break;
                case 'DESC':
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }
    
    // Function to safe redirect the page without warnings
    public function redirect( $url ) {
        echo '<script language="javascript">window.location.href="'.$url.'";</script>';
        exit();
    }
    
    //Function will get called on plugin activation
    static function als_install() {

        do_action('als_before_install');

        require_once ALS_PLUGIN_PATH.'includes/install.php';

        do_action('als_after_install');
    }

    // Function will get called on plugin de activation
    static function als_uninstall() {

        do_action('als_before_uninstall');

        require_once ALS_PLUGIN_PATH.'includes/uninstall.php';

        do_action('als_after_uninstall');
    }
}

$als = new ALS();