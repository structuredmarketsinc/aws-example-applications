<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Class that will hold functionality for admin side
 *
 * PHP version 5
 *
 * @category   Admin Side Code
 * @package    Lightsail API
 * @author     Muhammad Atiq
 * @version    1.0.0
 * @since      File available since Release 1.0.0
*/

class ALS_Admin extends ALS
{
    //Admin side starting point. Will call appropriate admin side hooks
    public function __construct() {
        
        do_action('als_before_admin', $this );
        //All admin side code will go here
        
        add_action( 'admin_menu', array( $this, 'als_admin_menus' ) );    
        //Action hook to show instances
        add_action( 'wp_ajax_als_list_instances', array( $this, 'als_list_instances') );
        
        //Action hook to get Zones options html
        add_action( 'wp_ajax_als_get_zones_html', array( $this, 'als_get_zones_html') );
        
        //Action hook to get Blueprints options html
        add_action( 'wp_ajax_als_get_blueprints_html', array( $this, 'als_get_blueprints_html') );
        
        //Action hook to get bundles options html
        add_action( 'wp_ajax_als_get_bundles_html', array( $this, 'als_get_bundles_html') );
        
        //Action hook to get SSH key options html
        add_action( 'wp_ajax_als_get_ssh_html', array( $this, 'als_get_ssh_html') );
        
        do_action('als_after_admin', $this );            
    }
    
    /*
     * Function to show admin menu
     */
    public function als_admin_menus(){
        
        add_menu_page( ALS_PLUGIN_NAME, ALS_PLUGIN_NAME, 'manage_options', 'als_settings', array( $this, 'als_settings' ) );
        add_submenu_page( 'als_settings', ALS_PLUGIN_NAME.' Settings', 'Settings', 'manage_options', 'als_settings', array( $this, 'als_settings' ) );
        add_submenu_page( 'als_settings', ALS_PLUGIN_NAME.' Instances', 'Instances', 'manage_options', 'als_instances', array( $this, 'als_instances' ) );
    }    
    
    /*
     * Settings page of the plugin
     */
    public function als_settings() {
        
        global $als_options, $als_lang;
        
        do_action('als_before_settings', $this, $als_options );
        $message = '';
        if( isset($_POST['btnsave']) && $_POST['btnsave'] != "" ) {            
            $exclude = array('btnsave');
            $als_options = array();            
            foreach( $_POST as $k => $v ) {
                if( !in_array( $k, $exclude )) {
                    if(!is_array($v)) {
                        $val = $this->make_safe($v);
                    }else{
                        $val = $v;
                    }
                    $als_options[$k] = $val;
                }
            }            
            update_option( 'als_settings', $als_options );
            
            //Load success message template
            $message = $this->als_get_message_html( __( 'Settings saved successfully!', 'als' ), 'message' );
        }
        if($_GET['als_testing'] == 'yes' ) {
            try{
                $this->als_do_testing();
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }
        $attr = $als_options;
        $attr['message'] = $message;
        //Load settings page template
        $html = $this->als_load_template( "settings", "admin", $attr );
        
        do_action('als_after_settings', $this, $als_options );
        
        echo $html;
    }
    
    /*
     * Ajax function to get the Key Pairs
     */
    public function als_get_ssh_html() {
        
        $message = $html = "";
        $error = false;
        $region = filter_input( INPUT_POST, 'region' );
        if( empty($region) ) {
            $error = true;
            $message = __( 'No region provided', 'als' );
        }
        $html = '<option value="">'.__( 'Select Key', 'als' ).'</option>';
        if( !$error ) {
            $region_arr  = explode( "|", $region );
            $region = $region_arr[0];
            $keys  = $this->als_get_key_pairs( $region );
            if(is_array($keys)) {
                foreach ( $keys as $key=>$val ) {
                    $html.='<option value="'.$val['name'].'">'.$val['name'].'</option>';
                }
            }
            $html.='<option value="als_create_new_ssh">'.__( 'Create New Key', 'als' ).'</option>';
        }
        
        $response = array( 'error' => $error, 'message' => $message, 'html' => $html );
        wp_send_json($response);
    }
    
    /*
     * Ajax function to get the Zones based on region
     */
    public function als_get_zones_html() {
        
        $message = $html = "";
        $error = false;
        $region = filter_input( INPUT_POST, 'region' );
        if( empty($region) ) {
            $error = true;
            $message = __( 'No region provided', 'als' );
        }
        $html = '<option value="">'.__( 'Select Zone', 'als' ).'</option>';
        if( !$error ) {
            $zones  = $this->als_get_zones( $region );
            if(is_array($zones)) {
                foreach ( $zones as $key=>$val ) {
                    $html.='<option value="'.$key.'">'.$val.'</option>';
                }
            }
        }
        
        $response = array( 'error' => $error, 'message' => $message, 'html' => $html );
        wp_send_json($response);
    }
    
    /*
     * Ajax function to get Blueprints based on the region
     */
    public function als_get_blueprints_html() {
        
        $message = $html = "";
        $error = false;
        $region = filter_input( INPUT_POST, 'region' );
        if( empty($region) ) {
            $error = true;
            $message = __( 'No region provided', 'als' );
        }
        $html = '<option value="">'.__( 'Select Blueprint', 'als' ).'</option>';
        if( !$error ) {
            $region_arr  = explode( "|", $region );
            $region = $region_arr[0];
            $blueprints  = $this->als_get_blueprints( $region );
            if(is_array($blueprints)) {
                foreach ( $blueprints as $key=>$val ) {
                    $html.='<option value="'.$key.'|'.$val['platform'].'">'.$val['name'].' ('.$key.')</option>';
                }
            }
        }
        
        $response = array( 'error' => $error, 'message' => $message, 'html' => $html );
        wp_send_json($response);
    }
    
    /*
     * Ajax function to get Bundles based on the region
     */
    public function als_get_bundles_html() {
        
        $message = $html = "";
        $error = false;
        $region     = filter_input( INPUT_POST, 'region' );
        $blueprint  = filter_input( INPUT_POST, 'blueprint' );
        if( empty($region) ) {
            $error = true;
            $message = __( 'No region provided', 'als' );
        }
        if( empty($blueprint) ) {
            $error = true;
            $message = __( 'No platform provided', 'als' );
        }
        $html = '<option value="">'.__( 'Select Bundle', 'als' ).'</option>';
        if( !$error ) {
            $region_arr  = explode( "|", $region );
            $region = $region_arr[0];
            $blueprint_arr  = explode( "|", $blueprint );
            $platform       = $blueprint_arr[1];
            $bundles  = $this->als_get_bundles( $region, $platform );
            if(is_array($bundles)) {
                foreach ( $bundles as $key=>$val ) {
                    $html.='<option value="'.$key.'">'.$val.'</option>';                    
                }
            }
        }
        
        $response = array( 'error' => $error, 'message' => $message, 'html' => $html );
        wp_send_json($response);
    }
    
    /*
     * Admin side page to show instances
     */
    public function als_instances() {
        
        $template = $html = $message = '';
        $action = filter_input( INPUT_GET, 'action' );
        $id     = filter_input( INPUT_GET, 'id' );
        
        if( $action == 'edit' && is_numeric( $id ) ) {
            //do edit
        }elseif( $action == 'add' ) {
            if( isset($_POST['btnsave']) && $_POST['btnsave'] != "" ) {            
                $message = $this->als_save_instance();
            }
            $regions = $this->als_get_all_regions_with_zones();
            $attr = array();
            $attr['regions'] = $regions;
            $template = "add_instance";  
        }else{
            if( $action == 'del' && is_numeric( $id ) ){
                $this->als_terminate_instance( $id );
            }elseif( $action == 'launch' && is_numeric( $id ) ){
                $message = $this->als_launch_instance( $id );
            }elseif( $action == 'start' && is_numeric( $id ) ){
                $message = $this->als_start_instance( $id );
            }elseif( $action == 'stop' && is_numeric( $id ) ){
                $message = $this->als_stop_instance( $id );
            }elseif( $action == 'reboot' && is_numeric( $id ) ){
                $message = $this->als_reboot_instance( $id );
            }elseif ($action == 'del' && $_REQUEST['ids'] != "" ){
                $ids = explode(",", $_REQUEST['ids']);
                if(is_array($ids)) {
                    foreach($ids as $id) {
                        if(is_numeric($id)) {
                            $this->als_terminate_instance( $id );
                        }
                    }
                }
            }
            wp_enqueue_script( 'als_jtable_js' );
            wp_enqueue_style( 'als_jtable_css' );
            $attr = array();
            $attr = $_REQUEST;
            $template = "instances";            
        }
        if( !empty($template) ) {
            $attr['message'] = $message;
            $html = $this->als_load_template( $template, "admin", $attr );
        }
        echo $html;
    }
    
    /*
     * Function to start an instance
     */
    public function als_start_instance( $id ) {
        
        if( !is_numeric($id) ) {
            //Load error message template
            $message = $this->als_get_message_html( __( 'You must provide instance id to start it!', 'als' ), 'error' );
            return $message;
        }
        
        $instance = $this->als_get_data("als_instances", "id = '".$id."'", true);
        try {   
            $result = $this->als_change_instance_state($instance->region, $instance->name, 'start' );
            if( empty($result) ) {
                //Load error message template
                $message = $this->als_get_message_html( __( 'There is some error to start this instance, please try again!', 'als' ), 'error' );
                return $message;
            }else{
                //Load success message template
                $message = $this->als_get_message_html( __( 'Instance Start successfully!', 'als' ), 'message' );
            }
        } catch (Exception $ex) {
            //Load error message template
            $message = $this->als_get_message_html( __( $ex->getMessage(), 'als' ), 'error' );
            return $message;
        }
        
        return $message;
    }
    
    /*
     * Function to stop an instance
     * 
     * @param $id interger ID of the instance in local database
     * 
     * @return $message html of message/error
     */
    public function als_stop_instance( $id ) {
        
        if( !is_numeric($id) ) {
            //Load error message template
            $message = $this->als_get_message_html( __( 'You must provide instance id to stop it!', 'als' ), 'error' );
            return $message;
        }
        
        $instance = $this->als_get_data("als_instances", "id = '".$id."'", true);
        try {   
            $result = $this->als_change_instance_state($instance->region, $instance->name, 'stop' );
            if( empty($result) ) {
                //Load error message template
                $message = $this->als_get_message_html( __( 'There is some error to stop this instance, please try again!', 'als' ), 'error' );
                return $message;
            }else{
                //Load success message template
                $message = $this->als_get_message_html( __( 'Instance Stoped successfully!', 'als' ), 'message' );
            }
        } catch (Exception $ex) {
            //Load error message template
            $message = $this->als_get_message_html( __( $ex->getMessage(), 'als' ), 'error' );
            return $message;
        }
        
        return $message;
    }
    
    /*
     * Function to stop an instance
     * 
     * @param $id interger ID of the instance in local database
     * 
     * @return $message html of message/error
     */
    public function als_reboot_instance( $id ) {
        
        if( !is_numeric($id) ) {
            //Load error message template
            $message = $this->als_get_message_html( __( 'You must provide instance id to reboot it!', 'als' ), 'error' );
            return $message;
        }
        
        $instance = $this->als_get_data("als_instances", "id = '".$id."'", true);
        try {   
            $result = $this->als_change_instance_state($instance->region, $instance->name, 'reboot' );
            if( empty($result) ) {
                //Load error message template
                $message = $this->als_get_message_html( __( 'There is some error to reboot this instance, please try again!', 'als' ), 'error' );
                return $message;
            }else{
                //Load success message template
                $message = $this->als_get_message_html( __( 'Instance reboot successfully!', 'als' ), 'message' );
            }
        } catch (Exception $ex) {
            //Load error message template
            $message = $this->als_get_message_html( __( $ex->getMessage(), 'als' ), 'error' );
            return $message;
        }
        
        return $message;
    }
    
    /*
     * Function to terminate an instance
     * 
     * @param $id interger ID of the instance in local database
     * 
     * @return $message html of message/error
     */
    public function als_terminate_instance( $id ) {
        
        if( !is_numeric($id) ) {
            //Load error message template
            $message = $this->als_get_message_html( __( 'You must provide instance id to terminate it!', 'als' ), 'error' );
            return $message;
        }
        
        $instance = $this->als_get_data("als_instances", "id = '".$id."'", true);
        try {   
            $result = $this->als_change_instance_state($instance->region, $instance->name, 'terminate' );
            if( empty($result) ) {
                //Load error message template
                $message = $this->als_get_message_html( __( 'There is some error to terminate this instance, please try again!', 'als' ), 'error' );
                return $message;
            }else{
                $this->als_del_record("als_instances", "id = '".$id."'");
                //Load success message template
                $message = $this->als_get_message_html( __( 'Instance terminated successfully!', 'als' ), 'message' );
            }
        } catch (Exception $ex) {
            //Load error message template
            $message = $this->als_get_message_html( __( $ex->getMessage(), 'als' ), 'error' );
            return $message;
        }
        
        return $message;
    }
    
    /*
     * Function to launch an existing instance or create new instance
     * 
     * @param $id interger ID of the instance in local database
     * 
     * @return $message html of message/error
     */
    public function als_launch_instance( $id ) {
        
        if( !is_numeric($id) ) {
            //Load error message template
            $message = $this->als_get_message_html( __( 'You must provide instance id to launch it!', 'als' ), 'error' );
            return $message;
        }
        $instance_id = "";
        
        try {        
            $instance = $this->als_get_data("als_instances", "id = '".$id."'", true);
            if( $instance ) {
                if( !empty($instance->instance_id) ) {
                    $als_instance = $this->als_get_instance( $instance->region, $instance->name );
                    if( !empty($als_instance) ) {
                        $instance_id = $als_instance['id'];
                    }
                }

                if( empty($instance_id) ) {                    
                    $als_instance = $this->als_create_instance( $instance->region, $instance->zone, $instance->blueprint, $instance->bundle, $instance->name, $instance->ssh );
                    if( !empty($als_instance) ) {                    
                        $instance_id = $als_instance['id']; 
                        $this->als_update_record( "als_instances", array( "private_key_path" => $als_instance['private_key_path'], "public_key_path" => $als_instance['public_key_path'], "instance_id" => $instance_id ), "id = '".$id."'" );
                    }
                }
            }

            if( empty($instance_id) ) {
                //Load error message template
                $message = $this->als_get_message_html( __( 'There is some error to launch the instance', 'als' ), 'error' );
                return $message;
            }else{
                //Load success message template
                $message = $this->als_get_message_html( __( 'Instance launched successfully!', 'als' ), 'message' );
            }
        }catch (Exception $ex) {
            //Load error message template
            $message = $this->als_get_message_html( __( $ex->getMessage(), 'als' ), 'error' );
            return $message;
        }
        
        return $message;
    }
    
    /*
     * Function to save instance data in database
     * 
     * @param $id interger ID of the instance in local database
     * 
     * @return $message html of message/error
     */
    public function als_save_instance( $id = 0 ) {
        
        $message = '';
        $exclude    = array('btnsave');
        $data       = $_POST;
        $region     = filter_input( INPUT_POST, 'region' );
        $ssh_new    = filter_input( INPUT_POST, 'ssh_new' );
        $blueprint  = filter_input( INPUT_POST, 'blueprint' );
        
        $region_arr  = explode( "|", $region );
        $data['region'] = $region_arr[0];
        $data['zone']   = $region_arr[1];
        
        $blueprint_arr      = explode( "|", $blueprint );
        $data['blueprint']  = trim($blueprint_arr[0]);
        $data['platform']   = trim($blueprint_arr[1]);
        
        if( !empty($ssh_new) && $data['ssh'] == "als_create_new_ssh" ) {
            $data['ssh']   = $ssh_new;            
        }
        $data['ssh'] = sanitize_title($data['ssh']); 
        unset( $data['ssh_new']);
        $data['name'] = sanitize_title($data['name']); 
        
        if( !$id && $this->als_add_record( "als_instances", $data ) ) {
            //Load success message template
            $message = $this->als_get_message_html( __( 'Instance saved successfully!', 'als' ), 'message' );
        }elseif( $id && $this->als_update_record("als_instances", $data, "id = '".$id."'") ) {
            //Load success message template
            $message = $this->als_get_message_html( __( 'Instance updated successfully!', 'als' ), 'message' );
        }else{
            //Load error message template
            $message = $this->als_get_message_html( __( 'There is some error to save the instance!', 'als' ), 'error' );
        }
        
        return $message;
    }
    
    /*
     * Ajax callback function to load instances
     */
    public function als_list_instances() {
        
        $sorting    = filter_input( INPUT_GET, 'jtSorting' );
        $start      = filter_input( INPUT_GET, 'jtStartIndex' );
        $page_size  = filter_input( INPUT_GET, 'jtPageSize' );
        $end        = $start+$page_size;
        
        $records = $this->als_get_data( "als_instances" ); 
        
        $recordCount = sizeof($records);
        if( $sorting != "" ) {
            $sorting_arr = explode(" ",$sorting);
            $orderby = trim($sorting_arr[0]);
            $order = trim($sorting_arr[1]);
        }
        
        $rows = $final_rows = array();
        $index = 0;
        foreach ( $records as $row ) {
            $state = "";
            $als_instance = null;            
            try { 
                $als_instance = $this->als_get_instance($row->region, $row->name);                
            }catch (Exception $ex) {
                
            }
            
            if( !empty($als_instance) ) {
                $state = $als_instance['state']['name'];//running|stopping|Failed|Completed|Succeeded
            }
            if($state == 'terminated' || $state == 'terminating') {
                $this->ec2_del_record("als_instances", "id = '".$row->id."'");
            }else{
                $actions = '';
                if( $state == '' ) {
                    $actions.='<td><a class="als_actions '.$state.'" title="Launch" href="admin.php?page=als_instances&action=launch&id='.$row->id.'">'.__( 'Launch', 'als' ).'</a></td>';
                }
                if( $state == 'running' || $state == 'pending' ) {
                    $actions.='<td><a class="als_actions '.$state.'" title="Stop" href="admin.php?page=als_instances&action=stop&id='.$row->id.'" onclick="javascript: return confirm(\'Are you sure you want to stop it?\');">'.__( 'Stop', 'als' ).'</a></td>';
                    $actions.='<td><a class="als_actions terminate" title="Reboot" href="admin.php?page=als_instances&action=reboot&id='.$row->id.'" onclick="javascript: return confirm(\'Are you sure you want to reboot it?\');">'.__( 'Reboot', 'als' ).'</a></td>';
                }
                if( $state == 'stopping' || $state == 'stopped' ) {
                    $actions.='<td><a class="als_actions '.$state.'" title="Start" href="admin.php?page=als_instances&action=start&id='.$row->id.'">'.__( 'Start', 'als' ).'</a></td>';
                }
                if( $state != '' && $state != 'terminated' && $state != 'terminating' ) {                                    
                    $actions.='<td><a class="als_actions terminate" title="Terminate" href="admin.php?page=als_instances&action=del&id='.$row->id.'" onclick="javascript: return confirm(\'Are you sure you want to terminate it?\');">'.__( 'Terminate', 'als' ).'</a></td>';
                }
                $record = array();
                $record['Action']       = '<table class="als_actions"><tr>'.$actions.'</tr></table>';
                $record['ID']           = $row->id;
                $record['Region']       = $row->region;
                $record['Blueprint']    = $row->blueprint;
                $record['SSH']          = $row->ssh;
                $record['Name']         = $row->name;
                $record['State']        = strtoupper($state);
                $rows[] = $record;
            }
            
        }
        
        $rows = $this->array_sort($rows, $orderby,$order);
        foreach ( $rows as $row ) {
            if($index >= $start && $index <= $end) {
                $final_rows[] = $row;
            }
            $index++;
        }
     
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = $recordCount;
        $jTableResult['Records'] = $final_rows;
        print json_encode($jTableResult);
        exit();
    }
}

$als_admin = new ALS_Admin();