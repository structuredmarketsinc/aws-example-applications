<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Class that will hold functionality for plugin activation
 *
 * PHP version 5
 *
 * @category   Install
 * @package    Lightsail API
 * @author     Muhammad Atiq
 * @version    1.0.0
 * @since      File available since Release 1.0.0
*/

class ALS_Install extends ALS
{
    public function __construct() {
        
        do_action('als_before_install', $this );
        
        global $wpdb;
        
        $sql="CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."als_instances` (
                `id` int(11) NOT NULL auto_increment,
                `region` varchar(250) DEFAULT NULL,
                `zone` varchar(250) DEFAULT NULL,
                `platform` varchar(250) DEFAULT NULL,
                `blueprint` varchar(250) DEFAULT NULL,                                
                `bundle` varchar(250) DEFAULT NULL,    
                `name` varchar(250) DEFAULT NULL,                    
                `instance_id` varchar(250) DEFAULT NULL,                
                `ssh` varchar(250) DEFAULT NULL,  
                `private_key_path` text NULL,
                `public_key_path` text NULL,
                PRIMARY KEY  (`id`)
              ) AUTO_INCREMENT=1 ;";

        $wpdb->query($sql);
        
        do_action('als_after_install', $this );
    }    
}

$als_install = new ALS_Install();