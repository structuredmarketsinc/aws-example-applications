<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * Class that will hold functionality for plugin deactivation
 *
 * PHP version 5
 *
 * @category   Uninstall
 * @package    Lightsail API
 * @author     Muhammad Atiq
 * @version    1.0.0
 * @since      File available since Release 1.0.0
*/

class ALS_Uninstall extends ALS
{
    public function __construct() {
        
        do_action('als_before_uninstall', $this );
        
        do_action('als_after_uninstall', $this );
    }    
}

$als_uninstall = new ALS_Uninstall();