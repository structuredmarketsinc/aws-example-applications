<?php
/*
Plugin Name: Lightsail API
Description: Amazon Lightsail API
Version: 1.0.0
Author: Muhammad Atiq
*/

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
    echo "Hi there!  I'm just a plugin, not much I can do when called directly.";
    exit;
}

define( 'ALS_PLUGIN_NAME', 'Lightsail' );
define( 'ALS_PLUGIN_PATH', plugin_dir_path(__FILE__) );
define( 'ALS_PLUGIN_URL', plugin_dir_url(__FILE__) );
define( 'ALS_SITE_BASE_URL',  rtrim(get_bloginfo('url'),"/")."/");
define( 'ALS_LANG_DIR', dirname( plugin_basename(__FILE__) ).'/language/' );

require_once ALS_PLUGIN_PATH.'includes/main.php';

register_activation_hook( __FILE__, array( 'ALS', 'als_install' ) );
register_deactivation_hook( __FILE__, array( 'ALS', 'als_uninstall' ) );