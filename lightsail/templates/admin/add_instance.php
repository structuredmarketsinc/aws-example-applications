<?php if ( $message!="" ) { echo $message; }?>
<div class="wrap">
<h2><?php echo __( 'Add Instance', 'als' );?></h2>
<table class="wp-list-table widefat fixed" cellspacing="0">
	<thead>
        <tr>
            <th scope="col" class="manage-column" style=""><?php echo __( 'Instance Detail', 'als' );?></th>
        </tr>
	</thead>
	<tbody id="the-list">
        <tr>
            <td>
            	<form method="post" name="frm_als" id="frm_als" class="frm_als" action="?page=als_instances&action=add" enctype="multipart/form-data">
                <table width="100%">
                    <tr>
                    	<td width="180"><?php echo __( 'Region', 'als' );?></td>
                        <td>
                            
                            <select name="region" id="region">
                                <option value=""><?php echo __( 'Select Region', 'als' );?></option>
                                <?php
                                /*
                                if(is_array($regions)) {
                                    foreach( $regions as $region ) {
                                        echo '<option value="'.$region['name'].'">'.$region['displayName'].' ( '.$region['name'].' )</option>';                                                                                
                                    }
                                }*/
                                if(is_array($regions)) {
                                    foreach( $regions as $region=>$zones ) {                                        
                                        echo '<optgroup label="'. $region .'">';
                                        foreach ( $zones as $key=>$val ) {
                                            echo '<option value="'.$region.'|'.$key.'">'.$val.'</option>';
                                        }
                                    }
                                }
                                ?>
                            </select>
                        </td>
                    </tr>
                    <?php /*?>
                    <tr class="hidden show_zones">
                    	<td><?php echo __( 'Zone', 'als' );?></td>
                        <td>
                            <select name="zone" id="zone">
                                <option value=""><?php echo __( 'Select Zone', 'als' );?></option>                               
                            </select>
                        </td>
                    </tr>
                    <?php */?>
                    <tr class="hidden show_amis">
                    	<td><?php echo __( 'Blueprint', 'als' );?></td>
                        <td>
                            <select name="blueprint" id="blueprint">
                                <option value=""><?php echo __( 'Select Blueprint', 'als' );?></option>                                
                            </select>
                        </td>
                    </tr>
                    <tr class="hidden show_instance_type">
                    	<td><?php echo __( 'Bundle', 'als' );?></td>
                        <td>
                            <select name="bundle" id="bundle">
                                <option value=""><?php echo __( 'Select Bundle', 'als' );?></option>                                
                            </select>
                        </td>
                    </tr>
                    <tr class="hidden showif_instance_type">
                    	<td><?php echo __( 'Instance Name', 'als' );?></td>
                        <td>
                            <input type="text" name="name" id="name" value="" />                            
                        </td>
                    </tr>
                    <tr class="hidden showif_instance_type">
                    	<td><?php echo __( 'SSH Key Name', 'als' );?></td>
                        <td>
                            <select name="ssh" id="ssh">
                                <option value=""><?php echo __( 'Select Key', 'als' );?></option>
                            </select>
                            <input type="text" class="hidden" name="ssh_new" id="ssh_new" value="" placeholder="<?php echo __( 'Put key name here', 'als' );?>" />                            
                        </td>
                    </tr>
                    <tr class="hidden showif_instance_type">
                        <td></td>
                        <td><input type="submit" name="btnsave" id="btnsave" value="<?php echo __( 'Add Instance', 'als' );?>" class="button button-primary">
                        </td>
                    </tr>
                </table>
                </form>
            </td>
        </tr>
     </tbody>
</table>
</div>