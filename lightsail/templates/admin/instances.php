<?php if ( $message!="" ) { echo $message; }?>
<style>
    .als_view_icon{margin-left:10px;width: 32px;height: 16px;font-size: 0px;background:url("<?php echo ALS_PLUGIN_URL;?>assets/images/view.png") no-repeat;display: inline-block;}
    .als_edit_icon{margin-left:10px;width: 24px;height: 24px;font-size: 0px;background:url("<?php echo ALS_PLUGIN_URL;?>assets/images/edit.png") no-repeat;display: inline-block;}
    .als_del_icon{margin-left:10px;width: 24px;height: 24px;font-size: 0px;background:url("<?php echo ALS_PLUGIN_URL;?>assets/images/del.png") no-repeat;display: inline-block;}
    .als_pdf_icon{margin-left:10px;width: 24px;height: 24px;font-size: 0px;background:url("<?php echo ALS_PLUGIN_URL;?>assets/images/pdf.png") no-repeat;display: inline-block;}
</style>
<div class="wrap">
    <br>
        <h1><?php echo __( 'Instances', 'als' );?> <a href="?page=als_instances&action=add" class="page-title-action">Add New</a></h1>
    <br>
    <div id="FieldsTableContainer" style="width: 100%;"></div>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('#FieldsTableContainer').jtable({
                title: 'Manage All Instances',
                paging: true,
                pageSize: 20,
                sorting: true,
                defaultSorting: 'ID ASC',
                actions: {
                    listAction: ajaxurl+'?action=als_list_instances'
                },
                fields: {
                        Action: {
                            title: 'Actions',
                            width: '20px',
                            sorting: false
                        },
                        ID: {
                            title: 'ID',
                            key: true,
                            create: false,
                            edit: false,
                            list: true,
                            width: '40px'
                        },
                        State: {
                            title: 'Current State',
                            width: '15%'
                        },
                        Region: {
                            title: 'Region',
                            width: '20%'
                        },
                        Blueprint: {
                            title: 'Blueprint',
                            width: '15%'
                        },
                        SSH: {
                            title: 'SSH Key',
                            width: '20%'
                        },
                        Name: {
                            title: 'Name',
                            width: '20%'
                        }
                }
            });
            //Load person list from server
            jQuery('#FieldsTableContainer').jtable('load');
            window.history.pushState("", "", '?page=als_instances');
        });
    </script>            
</div>