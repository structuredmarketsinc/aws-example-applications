<?php if ( $message!="" ) { echo $message; }?>
<div class="wrap">
<h2><?php echo __( 'Settings', 'als' );?></h2>
<table class="wp-list-table widefat fixed" cellspacing="0">
	<thead>
        <tr>
            <th scope="col" class="manage-column" style=""><?php echo __( 'Settings', 'als' );?></th>
        </tr>
	</thead>
	<tbody id="the-list">
        <tr>
            <td>
            	<form method="post" name="frm_als" id="frm_als" class="frm_als" action="?page=als_settings" enctype="multipart/form-data">
                <table width="100%">
                    <tr>
                    	<td width="180"><?php echo __( 'AWS Access Key Id', 'als' );?></td>
                        <td>
                            <input type="text" name="aws_access_key_id" id="aws_access_key_id" value="<?php echo $aws_access_key_id;?>" />
                        </td>
                    </tr>
                    <tr>
                    	<td><?php echo __( 'AWS Secret Key', 'als' );?></td>
                        <td>
                            <input type="text" name="aws_secret_key" id="aws_secret_key" value="<?php echo $aws_secret_key;?>" />
                        </td>
                    </tr>
                    
                    <tr>
                        <td></td>
                        <td><input type="submit" name="btnsave" id="btnsave" value="<?php echo __( 'Update', 'als' );?>" class="button button-primary">
                        </td>
                    </tr>
                </table>
                </form>
            </td>
        </tr>
     </tbody>
</table>
</div>