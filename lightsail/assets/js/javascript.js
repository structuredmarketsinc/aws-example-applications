jQuery(document).ready(function($){  
    
    //If rgion select box value changed
    if( $("#frm_als #region").length ) {
        $(document).on("change", '#frm_als #region', function(e) { 
            //Get blueprints
            als_get_blueprints_html($(this));
            als_get_ssh_html($(this));
        });
    }
    /*
    //If Zone select box value changed
    if( $("#frm_als #zone").length ) {
        $(document).on("change", '#frm_als #zone', function(e) { 
            //Get blueprints
            als_get_blueprints_html($(this));
        });
    }*/
    
    //If Blueprint select box value changed
    if( $("#frm_als #blueprint").length ) {
        $(document).on("change", '#frm_als #blueprint', function(e) { 
            //Get bundles
            als_get_bundles_html($(this));
        });
    }
    
    //If SSH key select box value changed
    if( $("#frm_als #ssh").length ) {
        $(document).on("change", '#frm_als #ssh', function(e) {             
            if( $(this).val() == "als_create_new_ssh" ) {
                $("#ssh_new").show();
            }else{
                $("#ssh_new").val("");
                $("#ssh_new").hide();
            }
        });
    }
});

/*
 * Show SSH key select box
 */
function als_get_ssh_html( obj ) {
    
    var $ = jQuery;
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: als_data.ajaxurl,
        data: {
                action: 'als_get_ssh_html',
                region: $("#frm_als #region").val()
        },
        success: function(response) {
            if(response.error) {
                alert(response.message);
            }else{
                $("#ssh").html(response.html);
            }
        },
        error: function (xhr, status, error) {
            alert("There is some error to connect with server. Please try again.");
        }
    }).always(function(){
               
    });
}

/*
 * Show Instance Types select box
 */
function als_get_bundles_html( obj ) {
    
    var $ = jQuery;
    show_ajax_loader(obj);
    $(".show_instance_type").hide();
    $(".showif_instance_type").hide();
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: als_data.ajaxurl,
        data: {
                action: 'als_get_bundles_html',
                region: $("#frm_als #region").val(),
                blueprint: $("#frm_als #blueprint").val()                       
        },
        success: function(response) {
            if(response.error) {
                alert(response.message);
            }else{
                $(".show_instance_type select").html(response.html);
                $(".show_instance_type").show();
                $(".showif_instance_type").show();
            }
        },
        error: function (xhr, status, error) {
            alert("There is some error to connect with server. Please try again.");
        }
    }).always(function(){
        hide_ajax_loader(obj);                
    });
}

/*
 * Show Zones select box
 */
function als_get_zones_html( obj ) {
    
    var $ = jQuery;
    show_ajax_loader(obj);
    $(".show_zones").hide();
    $(".show_amis").hide();
    $(".show_instance_type").hide();
    $(".showif_instance_type").hide();
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: als_data.ajaxurl,
        data: {
                action: 'als_get_zones_html',
                region: $("#frm_als #region").val()                     
        },
        success: function(response) {
            if(response.error) {
                alert(response.message);
            }else{
                $(".show_zones select").html(response.html);
                $(".show_zones").show();
            }
        },
        error: function (xhr, status, error) {
            alert("There is some error to connect with server. Please try again.");
        }
    }).always(function(){
        hide_ajax_loader(obj);                
    });
}

/*
 * Show AMIs select box
 */
function als_get_blueprints_html( obj ) {
    
    var $ = jQuery;
    show_ajax_loader(obj);
    $(".show_amis").hide();
    $(".show_instance_type").hide();
    $(".showif_instance_type").hide();
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: als_data.ajaxurl,
        data: {
                action: 'als_get_blueprints_html',
                region: $("#frm_als #region").val()                     
        },
        success: function(response) {
            if(response.error) {
                alert(response.message);
            }else{
                $(".show_amis select").html(response.html);
                $(".show_amis").show();
            }
        },
        error: function (xhr, status, error) {
            alert("There is some error to connect with server. Please try again.");
        }
    }).always(function(){
        hide_ajax_loader(obj);                
    });
}

//Show Ajax loader
function show_ajax_loader( obj ) {
    
    if(!obj.next('.als_ajax_loader').length) {
        obj.after('<div class="als_ajax_loader"></div>');
        obj.next('.als_ajax_loader').show();
    }
}

//Remove Ajax loader
function hide_ajax_loader( obj ) {
    
    if(obj.next('.als_ajax_loader').length) {
        obj.next('.als_ajax_loader').remove();
    }
}

//Delete multiple/all objects on listing page
function als_delete_all_selected(obj) {
    var $ = jQuery;
    var page = $(obj).attr("data-page");
    if(confirm('Are you sure you want to delete all selected items?')) {
        var ids = "";
        $(".als_select_item").each(function(){
            if($(this).is(":checked")){
                if(ids=="") {
                    ids = $(this).val();
                }else{
                    ids = ids+","+$(this).val();
                }
            }
        });
        if(ids=="") {
            alert("You must select some items to delete.");
        }else{
            window.location.href="admin.php?page="+page+"&action=del&ids="+ids;
        }
    }
    return false;
}

//Select all objects on listing page
function als_select_all(obj) {
    var $ = jQuery;
    if($(obj).is(":checked")) {
        $(".als_select_item").prop("checked",true);
    }else{
        $(".als_select_item").prop("checked",false);
    }
}